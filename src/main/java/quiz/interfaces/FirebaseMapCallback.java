package quiz.interfaces;
import java.util.*;

public interface FirebaseMapCallback
{
    void onCallback (Map<String, Object> prizeDisbutionMap);
}
