package quiz.data;

import java.util.Map;

public class PlayerData
{
    private String playerId = "";
    private String playerName = "";
    private String playerImageName = "";
    private String mobileNumber = "";
    private boolean isNameChanged = false;
    private int userType = 0; // 0 => Guest, 1 => verified
    private int lastMatchScore = 0;
    private int matchesPlayed = 0;
    private int totalEarned = 0;
    private int totalScore = 0;
    private int weeklyScore = 0;
    private int lastWeekScore = 0;
    private int cashoutStatus = 0;
    private int lastMatchEarned = 0;

    // These variables used for game
    private int pointsScored = 0;
//    private int prizeWon = 0; // needs to be removed in future
    private int remainingBalance = 0;
    private Map<String, Object> prizesMap;
    private Object balanceClaimsSnapshot = null;
    private Object prizeWonSnapshot = null;
    private Object easypaisaInfo = null;

    public String getPlayerId ()
    {
        return playerId;
    }

    public String getPlayerName ()
    {
        return playerName;
    }

    public String getPlayerImageName ()
    {
        return playerImageName;
    }

    public String getMobileNumber ()
    {
        return mobileNumber;
    }

    public boolean getNameChangedStatus ()
    {
        return isNameChanged;
    }

    public int getUserType ()
    {
        return userType;
    }

    public int getLastMatchEarned ()
    {
        return lastMatchEarned;
    }

    public int getLastMatchScore ()
    {
        return lastMatchScore;
    }

    public int getMatchesPlayed ()
    {
        return matchesPlayed;
    }

    public int getTotalEarned ()
    {
        return totalEarned;
    }

    public int getTotalScore ()
    {
        return totalScore;
    }

    public int getWeeklyScore ()
    {
        return weeklyScore;
    }

    public int getLastWeekScore ()
    {
        return lastWeekScore;
    }

    public int getCashoutStatus ()
    {
        return cashoutStatus;
    }

    public int getPointsScored ()
    {
        return pointsScored;
    }

//    public int getPrizeWon ()
//    {
//        return prizeWon;
//    }

    public int getRemainingBalance ()
    {
        return remainingBalance;
    }

    public Object getBalanceClaimsSnapshot ()
    {
        return balanceClaimsSnapshot;
    }

    public Map<String, Object> getPrizesMap ()
    {
        return prizesMap;
    }

    public Object getPrizeWonSnapshot ()
    {
        return prizeWonSnapshot;
    }

    public Object getEasypaisaInfo ()
    {
        return easypaisaInfo;
    }

    public void setPlayerId (String playerId)
    {
        this.playerId = playerId;
    }

    public void setPlayerName (String playerName)
    {
        this.playerName = playerName;
    }

    public void setPlayerImageName (String playerImageName)
    {
        this.playerImageName = playerImageName;
    }

    public void setNameChangedStatus (boolean isNameChanged)
    {
        this.isNameChanged = isNameChanged;
    }

    public void setUserType (int userType)
    {
        this.userType = userType;
    }

    public void setMobileNumber (String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public void setLastMatchEarned (int lastMatchEarned)
    {
        this.lastMatchEarned = lastMatchEarned;
    }

    public void setLastMatchScore (int lastMatchScore)
    {
        this.lastMatchScore = lastMatchScore;
    }

    public void setMatchesPlayed (int matchesPlayed)
    {
        this.matchesPlayed = matchesPlayed;
    }

    public void setTotalEarned (int totalEarned)
    {
        this.totalEarned = totalEarned;
    }

    public void setTotalScore (int totalScore)
    {
        this.totalScore = totalScore;
    }

    public void setWeeklyScore (int weeklyScore)
    {
        this.weeklyScore = weeklyScore;
    }

    public void setLastWeekScore (int lastWeekScore)
    {
        this.lastWeekScore = lastWeekScore;
    }

    public void setCashoutStatus (int cashoutStatus)
    {
        this.cashoutStatus = cashoutStatus;
    }

    public void setPointsScored (int pointsScored)
    {
        this.pointsScored = pointsScored;
    }

//    public void setPrizeWon (int prizeWon)
//    {
//        this.prizeWon = prizeWon;
//    }

    public void setRemainingBalance (int remainingBalance)
    {
        this.remainingBalance = remainingBalance;
    }

    public void setBalanceClaimsSnap (Object balanceClaimsSnap)
    {
        this.balanceClaimsSnapshot = balanceClaimsSnap;
    }

    public void setPrizeWonSnapshot (Object prizeWonSnapshot)
    {
        this.prizeWonSnapshot = prizeWonSnapshot;
    }

    public void setPrizesMap (Map<String, Object> prizesMap)
    {
        this.prizesMap = prizesMap;
    }

    public void setEasypaisaInfo (Object easypaisaInfo)
    {
        this.easypaisaInfo = easypaisaInfo;
    }

}
