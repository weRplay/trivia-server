package quiz.data;

import com.google.firebase.database.DataSnapshot;

public class NextGameData
{
    // Immutable data
    private String gameId = "";
    private long startTime = 0;
    private long endTime = 0;
    private int totalQuestions = 0;
    private int questionTime = 0;
    private int answerTime = 0;
    private int questionChangeTime = 0;
    private int gamePrize = 0;
    private int ranks = 10;
    private float multiplier = 2f;
    private DataSnapshot gamePrizeSnapshot;
    private int thresholdPoints = 0;
    private int defaultNotificationTime = 0;
    private boolean isProduction = false;
//    private String defaultNotificationTitle = "";
//    private String defaultNotificationMessage = "";

    private int quizNotificationTime = 0;
    private String quizNotificationTitle = "";
    private String quizNotificationMessage = "";

    private String quizKey = "";
    private Object gameImmutableData;
    private Object prodGameData;
    private DataSnapshot questionsSnapshot;
    private int adDuration = 0; // Ad duration per question
    // Mutable data
    private int countDown = 0;
    private int questionChangeCountDown = 0;
    private int adCountDown = 0;
    private int currentIndex = 0;
    private int remainingTime = 0;
    private boolean isShowingQuestionNumber = false;
    private boolean isShowingAnswer = false; // showAnswer in Db
    private boolean isShowingResults = false; // showResults in Db
    private boolean isCalculatingResults = false; // calculating resutls
    private boolean isPrizeDistributed = false; // prizeDistributed in Db
    private boolean isGameStarted = false; // isLive in Db
    private boolean isGamePasswordProtected = false;
    private boolean isAdDisplayed = false;
    // Quiz Type
    private DataSnapshot quizTypeSnapshot;

    /**
     * Initialize all Mutable variables
     * Reset the mutable data
     */
    public void initializeMutableData ()
    {
        countDown = 0;
        currentIndex = 0;
        remainingTime = 0;
        isShowingAnswer = false;
        isShowingResults = false;
        isGameStarted = false;
        isPrizeDistributed = false;
    }

    public String getGameId ()
    {
        return gameId;
    }

    public long getStartTime ()
    {
        return startTime;
    }

    public long getEndTime ()
    {
        return endTime;
    }

    public int getTotalQuestions ()
    {
        return totalQuestions;
    }

    public int getQuestionTime ()
    {
        return questionTime;
    }
    public int getAnswerTime ()
    {
        return answerTime;
    }

    public int getQuestionChangeTime ()
    {
        return questionChangeTime;
    }

    public int getQuizNotificationTime ()
    {
        return quizNotificationTime;
    }

    public String getQuizNotificationTitle ()
    {
        return quizNotificationTitle;
    }

    public String getQuizNotificationMessage ()
    {
        return quizNotificationMessage;
    }

    public int getDefaultNotificationTime ()
    {
        return defaultNotificationTime;
    }

//    public String getDefaultNotificationTitle ()
//    {
//        return defaultNotificationTitle;
//    }

//    public String getDefaultNotificationMessage ()
//    {
//        if (! defaultNotificationMessage.isEmpty ())
//        {
//            return defaultNotificationMessage;
//        }
//        else
//        {
//            return  "We're going live in " + getDefaultNotificationMessage () + " seconds.";
//        }
//    }


    public int getCountDown ()
    {
        return countDown;
    }

    public int getCurrentIndex ()
    {
        return currentIndex;
    }

    public int getRemainingTime ()
    {
        return remainingTime;
    }

    public int getGamePrize ()
    {
        return gamePrize;
    }

    public int getRanks ()
    {
        return ranks;
    }

    public float getMultiplier ()
    {
        return multiplier;
    }

    public DataSnapshot getGamePrizeSnapshot ()
    {
        return gamePrizeSnapshot;
    }

    public int getThresholdPoints ()
    {
        return thresholdPoints;
    }

    public boolean getProductionStatus ()
    {
        return isProduction;
    }

    public boolean getShowingQuestionNumberStatus ()
    {
        return isShowingQuestionNumber;
    }

    public boolean getShowingAnswerStatus ()
    {
        return isShowingAnswer;
    }

    public boolean getShowingResultsStatus ()
    {
        return isShowingResults;
    }

    public boolean getCalculatingResultsStatus ()
    {
        return isCalculatingResults;
    }

    public boolean getGameLiveStatus ()
    {
        return isGameStarted;
    }

    public boolean getGamePasswrodProtectedStatus ()
    {
        return isGamePasswordProtected;
    }

    public boolean getPrizeDistributedStatus ()
    {
        return isPrizeDistributed;
    }

    public String getQuizKey ()
    {
        return quizKey;
    }

    public Object getGameImmutableData ()
    {
        return gameImmutableData;
    }

    public Object getProdGameData ()
    {
        return prodGameData;
    }

    public DataSnapshot getQuestionsSnapshot ()
    {
        return questionsSnapshot;
    }

    public int getAdDuration ()
    {
        return adDuration;
    }

    public int getQuestionChangeCountDown ()
    {
        return questionChangeCountDown;
    }

    public int getAdCountDown ()
    {
        return adCountDown;
    }

    public boolean getAdDisplayStatus ()
    {
        return isAdDisplayed;
    }

    public DataSnapshot getQuizTypeSnapshot ()
    {
        return quizTypeSnapshot;
    }

    public void setGameId (String gameId)
    {
        this.gameId = gameId;
    }

    public void setStartTime (long startTime)
    {
        this.startTime = startTime;
    }

    public void setEndTime (long endTime)
    {
        this.endTime = endTime;
    }

    public void setTotalQuestions (int totalQuestions)
    {
        this.totalQuestions = totalQuestions;
    }

    public void setQuestionTime (int questionTime)
    {
        this.questionTime = questionTime;
    }

    public void setAnswerTime (int answerTime)
    {
        this.answerTime = answerTime;
    }

    public void setQuestionChangeTime (int changeQuestionTime)
    {
        this.questionChangeTime = changeQuestionTime;
    }


    public void setQuizNotificationTime (int quizNotificationTime)
    {
        this.quizNotificationTime = quizNotificationTime;
    }

    public void setQuizNotificationTitle (String quizNotificationTitle)
    {
        this.quizNotificationTitle = quizNotificationTitle;
    }

    public void setQuizNotificationMessage (String quizNotificationMessage)
    {
        this.quizNotificationMessage = quizNotificationMessage;
    }



    public void setDefaultNotificationTime (int defaultNotificationTime)
    {
        this.defaultNotificationTime = defaultNotificationTime;
    }

//    public void setDefaultNotificationTitle (String defaultNotificationTitle)
//    {
//        this.defaultNotificationTitle = defaultNotificationTitle;
//    }
//
//    public void setDefaultNotificationMessage (String defaultNotificationMessage)
//    {
//        this.defaultNotificationMessage = defaultNotificationMessage;
//    }

    public void setQuizKey (String quizKey)
    {
        this.quizKey = quizKey;
    }

    public void setGameImmutableData (Object gameData)
    {
        this.gameImmutableData = gameData;
    }

    public void setProdGameData (Object prodGameData)
    {
        this.prodGameData = prodGameData;
    }

    public void setCountDown (int countDown)
    {
        this.countDown = countDown;
    }

    public void setCurrentIndex (int currentIndex)
    {
        this.currentIndex = currentIndex;
    }

    public void setRemainingTime (int remainingTime)
    {
        this.remainingTime = remainingTime;
    }

    public void setShowingResultsStatus (boolean isShowingResults)
    {
        this.isShowingResults = isShowingResults;
    }

    public void setShowingQuestionNumberStatus (boolean isShowingQuestionNumber)
    {
        this.isShowingQuestionNumber = isShowingQuestionNumber;
    }

    public void setShowingAnswerStatus (boolean isShowingAnswer)
    {
        this.isShowingAnswer = isShowingAnswer;
    }

    public void setCalculatingResultsStatus (boolean isCalculatingResults)
    {
        this.isCalculatingResults = isCalculatingResults;
    }
    public void setGamePrize (int gamePrize)
    {
        this.gamePrize = gamePrize;
    }

    public void setGamePrizeSnapshot (DataSnapshot gamePrizeSnapshot)
    {
        this.gamePrizeSnapshot = gamePrizeSnapshot;
    }

    public void setRanks (int ranks)
    {
        this.ranks = ranks;
    }

    public void setMultiplier (float multiplier)
    {
        this.multiplier = multiplier;
    }

    public void setThresholdPoints (int thresholdPoints)
    {
        this.thresholdPoints = thresholdPoints;
    }

    public void setProductionStatus (boolean isProduction)
    {
        this.isProduction = isProduction;
    }

    public void setGameLiveStatus (boolean isGameStarted)
    {
        this.isGameStarted = isGameStarted;
    }

    public void setGamePasswordProtectedStatus (boolean isGamePasswordProtected)
    {
        this.isGamePasswordProtected = isGamePasswordProtected;
    }

    public void setPrizeDistributedStatus (boolean isPrizeDistributed)
    {
        this.isPrizeDistributed = isPrizeDistributed;
    }

    public void setQuestionsSnapshot (DataSnapshot questionsSnapshot)
    {
        this.questionsSnapshot = questionsSnapshot;
    }

    public void setAdDuration (int adDuration)
    {
        this.adDuration = adDuration;
    }

    public void setQuestionChangeCountDown (int questionChangeCountDown)
    {
        this.questionChangeCountDown = questionChangeCountDown;
    }

    public void setAdCountDown (int adCountDown)
    {
        this.adCountDown = adCountDown;
    }

    public void setAdDisplayedStatus (boolean isAdDisplayed)
    {
        this.isAdDisplayed = isAdDisplayed;
    }

    public void setQuizTypeSnapshot (DataSnapshot quizTypeSnapshot)
    {
        this.quizTypeSnapshot = quizTypeSnapshot;
    }
}

class GamePrizes
{
    public String prizeName;
    public String prizeImg;
    public String prizeMoney;
}