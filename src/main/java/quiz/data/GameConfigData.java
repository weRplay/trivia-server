package quiz.data;

public class GameConfigData
{
    private boolean isProduction = true;
    private long notificationTime = 0;
    private String notificationTitle = "";
    private String notificationMessage = "";

    public boolean getProductionStatus ()
    {
        return isProduction;
    }

    public long getNotificationTime ()
    {
        return notificationTime;
    }

    public String getNotificationTitle ()
    {
        return notificationTitle;
    }

    public String getNotificationMessage ()
    {
        return notificationMessage;
    }

    public void setProductionStatus (boolean isProduction)
    {
        this.isProduction = isProduction;
    }

    public void setNotificationTime (long notificationTime)
    {
        this.notificationTime = notificationTime;
    }

    public void setNotificationTitle (String notificationTitle)
    {
        this.notificationTitle = notificationTitle;
    }

    public void setNotificationMessage (String notificationMessage)
    {
        this.notificationMessage = notificationMessage;
    }
}
