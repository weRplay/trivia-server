package quiz.controllers;

import java.util.*;
import com.google.firebase.database.*;
import quiz.data.PlayerData;
import quiz.helpers.Constant;
import quiz.interfaces.Callback;
import quiz.helpers.ConversionHelper;
import quiz.MainApplication;


public class LeaderboardController
{
    // static variable of type Singleton
    private static LeaderboardController leaderboardControllerInstance = null;

    private PlayerData playerData;

    private Timer timer;
    private TimerTask weeklyResetTask;
    private long weeklyResetDelay = 7 * 86400000; // 7 days in milliseconds

    /**
     * LeaderboardController constructor
     */
    private LeaderboardController ()
    {
        Calendar calendar = Calendar.getInstance();
        System.out.printf ("calandar %s : %s %s \n", calendar.get(Calendar.DAY_OF_WEEK), calendar.get (Calendar.HOUR_OF_DAY), getWeeklyResetTime ());
        timer = new Timer ();
        weeklyResetTask = new TimerTask ()
        {
            @Override
            public void run ()
            {
                fetchAndResetWeeklyLeaderboard ();
            }
        };

        timer.scheduleAtFixedRate (weeklyResetTask, getWeeklyResetTime (), weeklyResetDelay);
//        timer.scheduleAtFixedRate (weeklyResetTask, 1000, 100000);
    }

    public static LeaderboardController getInstance ()
    {
        if (leaderboardControllerInstance == null)
            leaderboardControllerInstance = new LeaderboardController();
        return leaderboardControllerInstance;
    }

    /**
     * processLeaderbaord
     * Fetch the players performance in the last match
     * Update the leaderboard and Update the players profile
     */
    public void processLeaderbaord (String quizKey, boolean isPrivateGame, boolean isProduction, Callback callback)
    {
        System.out.println ("ProcessLeaderboard() "+ isPrivateGame + " production "+isProduction);
        MainApplication.currentGamePlayersDbRef.addListenerForSingleValueEvent (new ValueEventListener()
        {
            @Override
            public void onDataChange (DataSnapshot gameLeaderboardSnap)
            {
//                if (isProduction)
                    updateLeaderboard (quizKey, gameLeaderboardSnap, isPrivateGame);

                callback.onCallback ();
            }

            @Override
            public void onCancelled (DatabaseError error)
            {
                System.out.println ("procLeaderbaord () "+error);
            }
        });
    }

    public void updateSpecificGameLeaderboard (DatabaseReference dbReference, String quizKey)
    {
        dbReference.addListenerForSingleValueEvent (new ValueEventListener()
        {
            @Override
            public void onDataChange (DataSnapshot gameLeaderboardSnap)
            {
                System.out.println ("Debug: update specific game leaderboard");
                updateLeaderboard (quizKey, gameLeaderboardSnap, false);
            }

            @Override
            public void onCancelled (DatabaseError error)
            {
                System.out.println ("Debug: Error on update specific leaderboard "+error);
            }
        });
    }

    /**
     * getWeeklyResetTime
     * Day and Time to run for the first time
     * @return
     */
    private Date getWeeklyResetTime ()
    {
        short day = 2; // Monday
        Calendar calendar = Calendar.getInstance();

        calendar.set (Calendar.HOUR_OF_DAY, 0);
        calendar.set (Calendar.MINUTE, 0);
        calendar.set (Calendar.SECOND, 0);

        int diff = 7 - calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add (Calendar.DATE, diff+day);

        return calendar.getTime();
    }

    /**
     * updateLeaderbaord
     * @param gameLeaderboardSnap
     */
    private void updateLeaderboard (String quizKey, DataSnapshot gameLeaderboardSnap, Boolean isPrivate)
    {
        System.out.println("Debug: updateLeaderboard. quizkey: "+ quizKey +" leaderboards "+gameLeaderboardSnap );

        // Get the previous leaderboard data
        DatabaseReference leaderboardRef = MainApplication.firebaseDb.getReference("/leaderboard/");
        leaderboardRef.addListenerForSingleValueEvent (new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot leaderboardSnapshot)
            {
                Map<String, Object> playersDataMap = new HashMap<String, Object> ();

                for (DataSnapshot playerLastGameSnap : gameLeaderboardSnap.getChildren())
                {
                    try
                    {
                        Map<String, Object> prizesMap = new HashMap<> ();
                        // Initializing playerData class
                        playerData = new PlayerData ();

                        playerData.setPlayerId (playerLastGameSnap.getKey ());
                        if (playerLastGameSnap.hasChild (Constant.PlayerKeys.NAME))
                            playerData.setPlayerName (playerLastGameSnap.child (Constant.PlayerKeys.NAME).getValue ().toString ());
                        if (playerLastGameSnap.hasChild (Constant.PlayerKeys.IMAGE_NAME))
                            playerData.setPlayerImageName (playerLastGameSnap.child (Constant.PlayerKeys.IMAGE_NAME).getValue ().toString ());
                        if (playerLastGameSnap.hasChild (Constant.PlayerKeys.USER_TYPE))
                            playerData.setUserType (ConversionHelper.stringToInt (playerLastGameSnap.child (Constant.PlayerKeys.USER_TYPE).getValue ().toString ()));
                        if (playerLastGameSnap.hasChild (Constant.PlayerKeys.LAST_MATCH_EARNED)) // set last-match-earned amount, total-earned and remaining balance, if user has played the quiz for the first time, its totalEarned, lastMatchEarned and remainingBalance will be same
                        {
                            playerData.setLastMatchEarned (ConversionHelper.stringToInt (playerLastGameSnap.child (Constant.PlayerKeys.LAST_MATCH_EARNED).getValue ().toString ()));
                            playerData.setTotalEarned (ConversionHelper.stringToInt (playerLastGameSnap.child (Constant.PlayerKeys.LAST_MATCH_EARNED).getValue ().toString ()));
                            playerData.setRemainingBalance (ConversionHelper.stringToInt (playerLastGameSnap.child (Constant.PlayerKeys.LAST_MATCH_EARNED).getValue ().toString ()));
                        }
                        if (playerLastGameSnap.hasChild (Constant.PlayerKeys.POINTS_SCORED))
                        {
                            playerData.setTotalScore (ConversionHelper.stringToInt (playerLastGameSnap.child (Constant.PlayerKeys.POINTS_SCORED).getValue ().toString ()));
                        }
                        playerData.setMatchesPlayed (1);
                        playerData.setWeeklyScore (playerData.getTotalScore ());
                        playerData.setLastMatchScore (playerData.getTotalScore ());

                        if (playerLastGameSnap.hasChild (Constant.PlayerKeys.PRIZES_WON)) // distribute to top 3
                        {
                            playerData.setPrizeWonSnapshot (playerLastGameSnap.child (Constant.PlayerKeys.PRIZES_WON).getValue ());

                            prizesMap.put (quizKey, playerData.getPrizeWonSnapshot ());

                            playerData.setPrizesMap (prizesMap);
                        }

                        if (leaderboardSnapshot.hasChild (playerLastGameSnap.getKey ())) // if the player is already exist in leader-board, update its previous attributes
                        {
                            if (! isPrivate) // add player score to the previous score if the game isn't private
                            {
                                playerData.setTotalScore (playerData.getLastMatchScore () + ConversionHelper.stringToInt (leaderboardSnapshot.child (playerLastGameSnap.getKey ()).child (Constant.PlayerKeys.TOTAL_SCORE).getValue ().toString ()));
                                playerData.setWeeklyScore (playerData.getLastMatchScore () + ConversionHelper.stringToInt (leaderboardSnapshot.child (playerLastGameSnap.getKey ()).child (Constant.PlayerKeys.WEEKLY_SCORE).getValue ().toString ()));
                            }
                            else
                            {
                                playerData.setTotalScore (ConversionHelper.stringToInt (leaderboardSnapshot.child (playerLastGameSnap.getKey ()).child (Constant.PlayerKeys.TOTAL_SCORE).getValue ().toString ()));
                                playerData.setWeeklyScore (ConversionHelper.stringToInt (leaderboardSnapshot.child (playerLastGameSnap.getKey ()).child (Constant.PlayerKeys.WEEKLY_SCORE).getValue ().toString ()));
                            }

                            playerData.setMatchesPlayed (playerData.getMatchesPlayed () + ConversionHelper.stringToInt (leaderboardSnapshot.child (playerLastGameSnap.getKey ()).child (Constant.PlayerKeys.MATCHES_PLAYED).getValue ().toString ()));
                            playerData.setTotalEarned (playerData.getLastMatchEarned () + ConversionHelper.stringToInt (leaderboardSnapshot.child (playerLastGameSnap.getKey ()).child (Constant.PlayerKeys.TOTAL_EARNED).getValue ().toString ()));
                            if (leaderboardSnapshot.child (playerLastGameSnap.getKey ()).hasChild (Constant.PlayerKeys.REMAINING_BALANCE))
                                playerData.setRemainingBalance (playerData.getRemainingBalance () + ConversionHelper.stringToInt (leaderboardSnapshot.child (playerLastGameSnap.getKey ()).child (Constant.PlayerKeys.REMAINING_BALANCE).getValue ().toString ()));

                            for (DataSnapshot prizeWonSnapshot : leaderboardSnapshot.child (playerLastGameSnap.getKey ()).child (Constant.PlayerKeys.PRIZES_WON).getChildren ())
                            {
                                prizesMap.put (prizeWonSnapshot.getKey (), prizeWonSnapshot.getValue ());
                            }

                            playerData.setPrizesMap (prizesMap);
                        }

                        Map<String, Object> playerMap = new HashMap<String, Object> ();
                        playerMap.put (Constant.PlayerKeys.NAME, playerData.getPlayerName ());
                        playerMap.put (Constant.PlayerKeys.IMAGE_NAME, playerData.getPlayerImageName ());
                        playerMap.put (Constant.PlayerKeys.USER_TYPE, playerData.getUserType ());
                        playerMap.put (Constant.PlayerKeys.MATCHES_PLAYED, playerData.getMatchesPlayed ());
                        playerMap.put (Constant.PlayerKeys.LAST_MATCH_EARNED, playerData.getLastMatchEarned ());
                        playerMap.put (Constant.PlayerKeys.LAST_MATCH_SCORE, playerData.getLastMatchScore ());
                        playerMap.put (Constant.PlayerKeys.TOTAL_EARNED, playerData.getTotalEarned ());
                        playerMap.put (Constant.PlayerKeys.TOTAL_SCORE, playerData.getTotalScore ());
                        playerMap.put (Constant.PlayerKeys.WEEKLY_SCORE, playerData.getWeeklyScore ());
                        playerMap.put (Constant.PlayerKeys.LAST_WEEK_SCORE, playerData.getLastWeekScore ());
                        playerMap.put (Constant.PlayerKeys.PRIZES_WON, playerData.getPrizesMap ());
                        playerMap.put (Constant.PlayerKeys.REMAINING_BALANCE, playerData.getRemainingBalance ());

                        //                    playerMap.put ("easypaisaInfo", playerData.getEasypaisaInfo ());

                        playersDataMap.put (playerData.getPlayerId (), playerMap);

                        System.out.println ("update player profile");
                        // updating each user player profile so we don't lose the other parameters like setting, easypaisa info etc.
                        MainApplication.usersDbRef.child (playerLastGameSnap.getKey ()).updateChildren (playerMap, new DatabaseReference.CompletionListener ()
                        {
                            @Override
                            public void onComplete (DatabaseError databaseError, DatabaseReference databaseReference)
                            {
                                if (databaseError != null)
                                {
                                    System.out.println ("Data could not be saved " + databaseError.getMessage ());
                                }
                                else
                                {
                                    System.out.println ("player scores updated");
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        System.out.println("Debug: Exception while updating player leaderboard playerId: " + playerLastGameSnap.getKey () + " exception: " + e);
                    }
                }

                saveLeaderbaordData (playersDataMap);
            }

            @Override
            public void onCancelled(DatabaseError error)
            {
                System.out.println("Debug: onCancelled Data could not be update "+error);
            }
        });

    }

    /**
     * fetchAndResetWeeklyLeaderboard
     * Fetches all the data from leaderbaord, reset the weeklyScore to 0 and saves the data back in leaderboard reference
     */
    private void fetchAndResetWeeklyLeaderboard ()
    {
        MainApplication.leaderboardDbRef.addListenerForSingleValueEvent (new ValueEventListener ()
        {
            @Override
            public void onDataChange (DataSnapshot playersSnapshot)
            {
                System.out.println ("Firebase:request resetWeeklyLeaderboard");
                saveLeaderbaordData (resetWeeklyLeaderboard (playersSnapshot));
            }

            @Override
            public void onCancelled (DatabaseError error)
            {
                System.out.println("Firebase:Error resetWeeklyLeaderboard() " + error);
            }
        });
    }

    /**
     * resetWeeklyLeaderboard
     * @param playersSnapshot
     * @return
     */
    private Map resetWeeklyLeaderboard (DataSnapshot playersSnapshot)
    {
        Map<String, Object> playersDataMap = new HashMap<String, Object> ();

        for (DataSnapshot playerSnapshot : playersSnapshot.getChildren ())
        {
            try
            {
                playerData = new PlayerData ();
                playerData.setPlayerId (playerSnapshot.getKey ());
                if (playerSnapshot.hasChild (Constant.PlayerKeys.NAME))
                    playerData.setPlayerName (playerSnapshot.child (Constant.PlayerKeys.NAME).getValue ().toString ());
                if (playerSnapshot.hasChild (Constant.PlayerKeys.IMAGE_NAME))
                    playerData.setPlayerImageName (playerSnapshot.child (Constant.PlayerKeys.IMAGE_NAME).getValue ().toString ());
                if (playerSnapshot.hasChild (Constant.PlayerKeys.USER_TYPE))
                    playerData.setUserType (ConversionHelper.stringToInt (playerSnapshot.child (Constant.PlayerKeys.USER_TYPE).getValue ().toString ()));
                if (playerSnapshot.hasChild (Constant.PlayerKeys.MATCHES_PLAYED))
                    playerData.setMatchesPlayed (ConversionHelper.stringToInt (playerSnapshot.child (Constant.PlayerKeys.MATCHES_PLAYED).getValue ().toString ()));
                if (playerSnapshot.hasChild (Constant.PlayerKeys.LAST_MATCH_EARNED))
                    playerData.setLastMatchEarned (ConversionHelper.stringToInt (playerSnapshot.child (Constant.PlayerKeys.LAST_MATCH_EARNED).getValue ().toString ()));
                if (playerSnapshot.hasChild (Constant.PlayerKeys.LAST_MATCH_SCORE))
                    playerData.setLastMatchScore (ConversionHelper.stringToInt (playerSnapshot.child (Constant.PlayerKeys.LAST_MATCH_SCORE).getValue ().toString ()));
                if (playerSnapshot.hasChild (Constant.PlayerKeys.TOTAL_EARNED))
                    playerData.setTotalEarned (ConversionHelper.stringToInt (playerSnapshot.child (Constant.PlayerKeys.TOTAL_EARNED).getValue ().toString ()));
                if (playerSnapshot.hasChild (Constant.PlayerKeys.TOTAL_SCORE))
                    playerData.setTotalScore (ConversionHelper.stringToInt (playerSnapshot.child (Constant.PlayerKeys.TOTAL_SCORE).getValue ().toString ()));
                if (playerSnapshot.hasChild (Constant.PlayerKeys.WEEKLY_SCORE))
                    playerData.setLastWeekScore (ConversionHelper.stringToInt (playerSnapshot.child (Constant.PlayerKeys.WEEKLY_SCORE).getValue ().toString ()));
                if (playerSnapshot.hasChild (Constant.PlayerKeys.PRIZES_WON))
                    playerData.setPrizeWonSnapshot (playerSnapshot.child (Constant.PlayerKeys.PRIZES_WON).getValue ());
                if (playerSnapshot.hasChild (Constant.PlayerKeys.IS_NAME_CHANGED))
                    playerData.setNameChangedStatus (ConversionHelper.stringToBool (playerSnapshot.child (Constant.PlayerKeys.IS_NAME_CHANGED).getValue ().toString ()));
                if (playerSnapshot.hasChild (Constant.PlayerKeys.REMAINING_BALANCE))
                    playerData.setRemainingBalance (ConversionHelper.stringToInt (playerSnapshot.child (Constant.PlayerKeys.REMAINING_BALANCE).getValue ().toString ()));
                if (playerSnapshot.hasChild (Constant.PlayerKeys.BALANCE_CLAIMS))
                    playerData.setBalanceClaimsSnap (playerSnapshot.child (Constant.PlayerKeys.BALANCE_CLAIMS).getValue ());

                // Reset the weekly score back to zero
                playerData.setWeeklyScore (0);

                Map<String, Object> playerMap = new HashMap<String, Object> ();
                playerMap.put (Constant.PlayerKeys.NAME, playerData.getPlayerName ());
                playerMap.put (Constant.PlayerKeys.IMAGE_NAME, playerData.getPlayerImageName ());
                playerMap.put (Constant.PlayerKeys.USER_TYPE, playerData.getUserType ());
                playerMap.put (Constant.PlayerKeys.MATCHES_PLAYED, playerData.getMatchesPlayed ());
                playerMap.put (Constant.PlayerKeys.LAST_MATCH_EARNED, playerData.getLastMatchEarned ());
                playerMap.put (Constant.PlayerKeys.LAST_MATCH_SCORE, playerData.getLastMatchScore ());
                playerMap.put (Constant.PlayerKeys.TOTAL_EARNED, playerData.getTotalEarned ());
                playerMap.put (Constant.PlayerKeys.TOTAL_SCORE, playerData.getTotalScore ());
                playerMap.put (Constant.PlayerKeys.WEEKLY_SCORE, playerData.getWeeklyScore ());
                playerMap.put (Constant.PlayerKeys.LAST_WEEK_SCORE, playerData.getLastWeekScore ());
                playerMap.put (Constant.PlayerKeys.PRIZES_WON, playerData.getPrizeWonSnapshot ());
                playerMap.put (Constant.PlayerKeys.IS_NAME_CHANGED, playerData.getNameChangedStatus ());
                playerMap.put (Constant.PlayerKeys.REMAINING_BALANCE, playerData.getRemainingBalance ());
                playerMap.put (Constant.PlayerKeys.BALANCE_CLAIMS, playerData.getBalanceClaimsSnapshot ());

                playersDataMap.put (playerData.getPlayerId (), playerMap);
            }
            catch (Exception e)
            {
                System.out.println ("Exception: "+e);
            }
        }

        return playersDataMap;
    }

    /**
     * saveLeaderboardMap
     *
     * @param leaderbaordDataMap
     */
    private void saveLeaderbaordData (Map leaderbaordDataMap)
    {
        MainApplication.leaderboardDbRef.updateChildren (leaderbaordDataMap, new DatabaseReference.CompletionListener()
        {
            @Override
            public void onComplete (DatabaseError databaseError, DatabaseReference databaseReference)
            {
                if (databaseError != null)
                {
                    System.out.println ("Firebase:Error saving leaderbaord " + databaseError.getMessage());
                }
                else
                {
                    System.out.println("Firebase:Success Leaderboard saved successfully "+leaderbaordDataMap);
                }
            }
        });
    }

    /**
     * userUserProfile
     * @param leaderbaordDataMap
     */
    private void updateUserProfileDB (Map leaderbaordDataMap)
    {

        MainApplication.usersDbRef.updateChildren (leaderbaordDataMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference)
            {
                if (databaseError != null)
                {
                    System.out.println("Data could not be saved " + databaseError.getMessage());
                }
                else
                {
                    System.out.println("player scores updated");
                }
            }
        });
    }

}
