package quiz.controllers;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerController
{
    private static long currentTime = 1000;

    public TimerController ()
    {
        Timer timer = new Timer ();

        TimerTask timerTask = new TimerTask()
        {
            @Override
            public void run ()
            {
                currentTime = new Date ().getTime ();
            }
        };

        timer.scheduleAtFixedRate (timerTask, 0, 1000);
    }

    public static long getCurrentTime ()
    {
        return currentTime;
    }
}
