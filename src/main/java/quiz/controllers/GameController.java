package quiz.controllers;

import com.google.firebase.database.*;
import quiz.data.GameConfigData;
import quiz.data.PlayerData;
import quiz.data.NextGameData;
import quiz.helpers.Constant;
import quiz.helpers.ConversionHelper;
import quiz.MainApplication;
import quiz.helpers.LogToFile;
import quiz.interfaces.FirebaseMapCallback;

import java.util.*;
import java.util.Timer;

public class GameController
{
    // Private  variables
    private HashMap<String, Object> countDownMap;
    private int count = 0;
    private boolean defaultPnBeforeGame = false;
    private boolean pnOnSpecifiedTime = false;

    // Private Reference variables
    private Timer timer; // Timer class for scheduling game loop
    private NextGameData gameData;
    private LeaderboardController leaderboardController;
    private GameConfigData gameConfigData;

    /**
     * resetReferences
     * Update the nextGameData reference and reset the leaderboarController reference
     * Reset the remaining-time to 60, push
     * @param gameData
     * @param leaderboardController
     */
    public void resetReferences (NextGameData gameData, LeaderboardController leaderboardController, GameConfigData gameConfigData)
    {
        this.gameData = gameData;
        this.leaderboardController = leaderboardController;
        this.gameConfigData = gameConfigData;

        this.gameData.setRemainingTime (60);
        this.defaultPnBeforeGame = false;
        this.pnOnSpecifiedTime = false;

        System.out.println ("Check timer task "+this.timer);
        if (this.timer != null)
            this.stopTimerTask();

        System.out.println ("Run timer task again"+this.timer);
        if (!gameData.getQuizKey ().isEmpty ())
            this.runTimerTask ();
    }

    /**
     * runTimerTask
     * Main game-loop, it starts working when it receives the next-game to run
     * Sends the push notification on specified time in gameData and also sends the notification before 2-min of the game start
     * Starts, handle and end the game
     */
    private void runTimerTask ()
    {
        System.out.println ("Debug: Game Config Data "+gameConfigData.getNotificationTime ());
        System.out.printf ("currentTime: %s startTime: %s endTime: %s \n", TimerController.getCurrentTime (), gameData.getStartTime (), gameData.getEndTime ());
        count = 0;
        timer = new Timer();
        TimerTask timerTask = new TimerTask ()
        {
            @Override
            public void run ()
            {
                count++;
//                System.out.println ("Comparing time" + count);
//                if (gameData.getProductionStatus ())
//                {
                    if (! gameData.getGamePasswrodProtectedStatus ())
                    {
                        // Send the notification on specified time
                        if (gameData.getQuizNotificationTime () > 0 && TimerController.getCurrentTime () >= gameData.getStartTime () - (gameData.getQuizNotificationTime ()) && TimerController.getCurrentTime () < gameData.getStartTime () && !pnOnSpecifiedTime)
                        {
                            System.out.println ("pn on specified time");
                            PushNotificationController.sendPushNotification (gameData.getQuizNotificationTitle (), gameData.getQuizNotificationMessage (), gameData.getProductionStatus ());
                            pnOnSpecifiedTime = true;
                        }
                        else if (gameConfigData.getNotificationTime () > 0 && TimerController.getCurrentTime () >= (gameData.getStartTime () - gameConfigData.getNotificationTime ()) && TimerController.getCurrentTime () < gameData.getStartTime () && !defaultPnBeforeGame) // Send notification on the specified time defined in game config
                        {
                            System.out.println ("pn on default time in config");
                            PushNotificationController.sendPushNotification (gameConfigData.getNotificationTitle (), gameConfigData.getNotificationMessage (), gameData.getProductionStatus ());
                            defaultPnBeforeGame = true;
                        }
                    }
//                }

                // Start remaining time 61 sec before
                if (TimerController.getCurrentTime() >= gameData.getStartTime() - ConversionHelper.secToMillisec (61) && TimerController.getCurrentTime() <= gameData.getStartTime())
                {
                    System.out.println("countdown to : " + gameData.getRemainingTime());
                    gameData.setRemainingTime (gameData.getRemainingTime()-1);
                    showRemainingTime (gameData.getRemainingTime());
                }
                // subtracting a second(1000), so everyone is able to get the countdown from 10 sec on first question
                if (TimerController.getCurrentTime() >= gameData.getStartTime()-1000 && TimerController.getCurrentTime () <= gameData.getEndTime() &&  gameData.getCurrentIndex() < gameData.getTotalQuestions())
                {
                    // System.out.println ("time to start : ");
                    startGame ();
                    handleGameplayLoop ();

                }
                else if (TimerController.getCurrentTime() >= gameData.getEndTime() && gameData.getCurrentIndex() == gameData.getTotalQuestions() - 1 && gameData.getGameLiveStatus ())
                {
                    endGame();
                    stopTimerTask();
                }
            }
        };

        timer.scheduleAtFixedRate (timerTask, 0, 1000);

    }

    /**
     * stopTimerTask
     * stop the timer task class when the game ends
     */
    public void stopTimerTask ()
    {
        System.out.println ("timer task is terminated");
        timer.cancel();
        timer = null;
    }

    /**
     * showRemainingTime
     * shows the count-down of 60 second before the game starts
     * @param remainingTime
     */
    public void showRemainingTime (int remainingTime)
    {

        HashMap<String, Object> showCountDownMap = new HashMap <String, Object>();
        showCountDownMap.put ("remainingTime",  remainingTime);
        showCountDownMap.put ("preMatch", true);

        updateMutableData (showCountDownMap, null);
    }

    /**
     * startGame
     * Starts the game and update the initial data require to start the game in db
     */
    private void startGame ()
    {
        if (!gameData.getGameLiveStatus ())
        {
            // Set the initial values for starting the game
            gameData.setCountDown (gameData.getQuestionTime());
            gameData.setQuestionChangeCountDown (gameData.getQuestionChangeTime ());
            gameData.setCurrentIndex (0);
            gameData.setShowingResultsStatus (false);
            gameData.setShowingAnswerStatus (false);
            gameData.setShowingQuestionNumberStatus (true);
            gameData.setGameLiveStatus (true);

            // Save the game starting data in db
            HashMap<String, Object> startGameMap = new HashMap <String, Object> ();
            startGameMap.put ("currentIndex", gameData.getCurrentIndex());
            startGameMap.put ("countDown", gameData.getQuestionTime());
            startGameMap.put ("showAnswer", gameData.getShowingAnswerStatus());
            startGameMap.put ("showResults", gameData.getShowingResultsStatus());
            startGameMap.put ("serverTime", TimerController.getCurrentTime());
            startGameMap.put (Constant.GameKeys.SHOW_QUESTION_NUMBER, gameData.getShowingQuestionNumberStatus ());
            startGameMap.put ("isLive", gameData.getGameLiveStatus ());

            updateMutableData (startGameMap, null);
        }
    }

    /**
     * handleCountDown
     * update the database states
     */
    private void handleGameplayLoop ()
    {
        countDownMap = new HashMap<String, Object>();

        if (gameData.getQuestionChangeCountDown () > 0) // show question number popup
        {
            gameData.setQuestionChangeCountDown (gameData.getQuestionChangeCountDown () -1);
        }
        else if (gameData.getQuestionChangeCountDown () == 0 && gameData.getShowingQuestionNumberStatus ()) // show questions
        {
            gameData.setCountDown (gameData.getQuestionTime ());
            gameData.setShowingQuestionNumberStatus (false);
            gameData.setShowingAnswerStatus (false);
            gameData.setAdDisplayedStatus (false);

            countDownMap.put (Constant.GameKeys.SHOW_QUESTION_NUMBER, gameData.getShowingQuestionNumberStatus ());
            countDownMap.put (Constant.GameKeys.SHOW_ANSWER, gameData.getShowingAnswerStatus ());
            countDownMap.put (Constant.GameKeys.COUNT_DOWN, gameData.getCountDown());
            countDownMap.put (Constant.GameKeys.SHOW_AD, gameData.getAdDisplayStatus ());

            gameData.setCountDown (gameData.getQuestionTime () - 1);
        }
        else if (gameData.getCountDown() <= gameData.getQuestionTime() && gameData.getCountDown() > 0) // subtract countdown
        {
            countDownMap.put (Constant.GameKeys.COUNT_DOWN, gameData.getCountDown());

            gameData.setCountDown (gameData.getCountDown() - 1);
        }
        else if (gameData.getCountDown() == 0) // show the answer to user
        {
            System.out.println ("getCountDown 0");
            gameData.setShowingAnswerStatus (true);

            countDownMap.put (Constant.GameKeys.COUNT_DOWN, gameData.getCountDown ());
            countDownMap.put (Constant.GameKeys.SHOW_ANSWER, gameData.getShowingAnswerStatus ());

            // Decrement the countdown
            gameData.setCountDown (gameData.getCountDown () - 1);
        }
        else if (gameData.getAdCountDown () > 0) // decrement ad countdown
        {
            gameData.setAdCountDown (gameData.getAdCountDown ()-1);

            countDownMap.put (Constant.GameKeys.AD_COUNT_DOWN, gameData.getAdCountDown ());
            countDownMap.put (Constant.GameKeys.SHOW_AD, true);
            if (gameData.getAdCountDown () == 0)
                gameData.setAdDisplayedStatus (true);
        }
        else if (gameData.getCountDown() == Constant.GameplaySettings.ANSWER_TIME_END) // show ad if exist otherwise increment the question index
        {
            if (gameData.getQuestionsSnapshot ().child (ConversionHelper.intToString (gameData.getCurrentIndex ())).hasChild (Constant.GameKeys.AD) && !gameData.getAdDisplayStatus ())
            {
                gameData.setAdDuration (ConversionHelper.stringToInt (gameData.getQuestionsSnapshot ().child (ConversionHelper.intToString (gameData.getCurrentIndex ())).child (Constant.GameKeys.AD).child (Constant.GameKeys.AD_DURATION).getValue ().toString ()));
                gameData.setAdCountDown (gameData.getAdDuration ());
            }
            else
            {

                // Increment the current-questions-index if questions left
                if (gameData.getCurrentIndex () < gameData.getTotalQuestions () - 1)
                {
                    System.out.println ("getCountDown == -2");
                    gameData.setShowingQuestionNumberStatus (true);
                    gameData.setQuestionChangeCountDown (gameData.getQuestionChangeTime ());
                    gameData.setCurrentIndex (gameData.getCurrentIndex () + 1); // increment question number
                }

                countDownMap.put (Constant.GameKeys.SHOW_QUESTION_NUMBER, gameData.getShowingQuestionNumberStatus ());
                countDownMap.put (Constant.GameKeys.CURRENT_INDEX, gameData.getCurrentIndex ());
            }
        }
        else // decrement the countdown
        {
            gameData.setCountDown (gameData.getCountDown()-1);
        }

        // Update database states
        updateMutableData (countDownMap, null);

    }

    /**
     * endGame
     */
    private void endGame ()
    {
        try
        {
            // Set the gameData values for ending the game
            gameData.setCalculatingResultsStatus (true);
            gameData.setShowingResultsStatus (true);
            gameData.setPrizeDistributedStatus (false);
            gameData.setGameLiveStatus (false);

            // Update states in db
            HashMap<String, Object> endGameMap = new HashMap<String, Object> ();
            endGameMap.put ("isCalculatingResults", gameData.getCalculatingResultsStatus ());
            endGameMap.put ("showResults", gameData.getShowingResultsStatus ());
            endGameMap.put ("prizeDistributed", gameData.getPrizeDistributedStatus ());
            endGameMap.put ("isLive", gameData.getGameLiveStatus ());

            updateMutableData (endGameMap, new FirebaseMapCallback ()
            {
                @Override
                public void onCallback (Map<String, Object> prizeDisbutionMap)
                {
                    System.out.println ("game has been ended");

                    int distributionType = ConversionHelper.stringToInt (gameData.getQuizTypeSnapshot ().child ("distributionType").getValue ().toString ());
                    if (distributionType == Constant.DistributionType.DistributeToAll.ordinal () || distributionType == Constant.DistributionType.DistributeToRanks.ordinal ())
                    {
                        // Calculate the total points scored by users and distribute the prize between those who crossed threshold or Distribute the prize to top ranks
                        fetchPlayerPoints ();
                        System.out.println ("Distribution type 0 or 2");
                    }
                    else
                    {
                        System.out.println ("Distribute to top 3");
                        // Delete guest users from game
                        // deleteGuestUsers ();

                        // Distribute to top 3
                        fetchTopThree ();
                    }
                }
            });
        }
        catch (Exception e)
        {
            LogToFile.logGameExceptions ("Exception: endGame (). exception: " + e);
        }
    }

    private void processAfterResult (Map<String, Object> playersDataMap)
    {
        System.out.println ("Debug: Process After Result. playersDataMap "+playersDataMap);
        // Update calculating results status
        gameData.setCalculatingResultsStatus (false);

        HashMap<String, Object> mutableDataMap = new HashMap<String, Object> ();
        mutableDataMap.put ("isCalculatingResults", gameData.getCalculatingResultsStatus ());

        updateMutableData (mutableDataMap, null);

        // Update leaderboard
        leaderboardController.processLeaderbaord (gameData.getQuizKey (), gameData.getGamePasswrodProtectedStatus (), gameData.getProductionStatus (), () ->
        {
            // Update Played Games Db
            updatePlayedGames (gameData.getQuizKey (), gameData.getGameImmutableData (), playersDataMap);
            // Remove the previous game from Db
            removePreviousGame (gameData.getQuizKey ());
        });
    }

    /**
     * fetchPlayerPoints()
     * Fetch all the players,
     * CalculateTotalPoints and call distributePrize with total points, fetched players snapshot, threshold points and game prize as a arguments
     * Update Leaderboard with fetched players
     * Remove the game from nextGames and update the playedGames Db
     */
    private void fetchPlayerPoints ()
    {
        try
        {
//            MainApplication.currentGamePlayersDbRef.orderByChild (Constant.PlayerKeys.POINTS_SCORED).startAt (gameData.getThresholdPoints ()+1).addListenerForSingleValueEvent (new ValueEventListener ()
//            MainApplication.currentGamePlayersDbRef.addListenerForSingleValueEvent (new ValueEventListener ()
            MainApplication.currentGamePlayersDbRef.orderByChild (Constant.PlayerKeys.POINTS_SCORED).addListenerForSingleValueEvent (new ValueEventListener ()
            {
                @Override
                public void onDataChange (DataSnapshot playersSnapshot)
                {
                    int distributionType = ConversionHelper.stringToInt (gameData.getQuizTypeSnapshot ().child ("distributionType").getValue ().toString ());
                    // If threshold points are set to zero, that mean the game is set for distributing among top 10
                    if (distributionType == Constant.DistributionType.DistributeToRanks.ordinal ())
                    {
                        distributePrize (gameData.getRanks (), gameData.getMultiplier (), playersSnapshot, gameData.getGamePrize (), GameController.this::processAfterResult);
                    }
                    else
                    {
                        // Distribute Prize
                        distributePrize (calculateTotalPoints (playersSnapshot), playersSnapshot, gameData.getThresholdPoints (), gameData.getGamePrize (), GameController.this::processAfterResult);
                    }
                }

                @Override
                public void onCancelled (DatabaseError error)
                {
                    LogToFile.logGameExceptions ("DatabaseError onCancelled: fetchPlayerPoints() while fetching player record. error: " + error);
                }
            });
        }
        catch (Exception e)
        {
            LogToFile.logGameExceptions ("Exception: FetchPlayerPoints (). exception: " + e);
        }
    }

    /**
     * calculateTotalPoints
     * Sums up the points scored by all the players who crossed the threshold and returns the total score value
     * @param playersSnapshot
     * @return
     */
    private int calculateTotalPoints (DataSnapshot playersSnapshot)
    {
        try
        {
            System.out.println ("get total score");
            int totalScore = 0;
            int pointScored = 0;

            for (DataSnapshot player : playersSnapshot.getChildren ())
            {
                if (player.hasChild (Constant.PlayerKeys.POINTS_SCORED))
                    pointScored = ConversionHelper.stringToInt (player.child (Constant.PlayerKeys.POINTS_SCORED).getValue ().toString ());

                if (pointScored >= gameData.getThresholdPoints ())
                {
                    totalScore += pointScored;
                }
            }

            System.out.println ("Debug: Total points:  " + totalScore);
            return totalScore;
        }
        catch (Exception e)
        {
            LogToFile.logGameExceptions ("Exception: calculateTotalPoints. exception: " + e);
            return 0;
        }
    }

//    private HashMap<Integer, HashMap<String, DataSnapshot>> getWinnersMap (int totalRanks, DataSnapshot playersSnapshot)
//    {
//        System.out.printf ("Debug: getWinnersMap. totalRanks: %s \n", totalRanks);
//
//        HashMap<Integer, HashMap<String, DataSnapshot>> winnersMap = new HashMap<> ();
//
//        int currentRank = totalRanks;
//        int playerPoints = 0;
//        int prevPlayerPoints = 0;
//
//        for( DataSnapshot playerSnapshot : playersSnapshot.getChildren ())
//        {
//            try
//            {
//                HashMap<String, DataSnapshot> playerMap = new HashMap<> ();
//                playerMap.put (playerSnapshot.getKey (), playerSnapshot);
//
//                if (playerSnapshot.hasChild (Constant.PlayerKeys.POINTS_SCORED))
//                    playerPoints = ConversionHelper.stringToInt (playerSnapshot.child (Constant.PlayerKeys.POINTS_SCORED).getValue ().toString ());
//
//                System.out.printf ("Debug: getWinnersMap. current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);
//
//                if (playerPoints == prevPlayerPoints)
//                {
//                    System.out.printf ("Debug: getWinnersMap. Players has same points. playerPoints: %s prevPlayerPoints: %s \n", playerPoints, prevPlayerPoints);
//                    currentRank += 1;
//                    if (winnersMap.containsKey (currentRank))
//                        winnersMap.get (currentRank).put (playerSnapshot.getKey (), playerSnapshot);
//                    else
//                        winnersMap.put (currentRank, playerMap);
//                }
//                else if (playerPoints < prevPlayerPoints && winnersMap.size () == totalRanks)
//                {
//                    System.out.printf ("Debug: getWinnersMap. ranks completed current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);
//                    break;
//                }
//                else
//                {
//                    winnersMap.put (currentRank, playerMap);
//                    currentRank -= 1;
//                    System.out.printf ("Debug: getWinnersMap. update rank current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);
//                }
//
////                System.out.printf ("Debug: getWinnersMap. currentRank: %s playerPoints: %s prevPlayerPoints: %s rankDictSize: %s winnersDictSize: %s \n", currentRank, playerPoints, prevPlayerPoints, winnersMap.get (currentRank).size (), winnersMap.size ());
//                prevPlayerPoints = playerPoints;
//            }
//            catch (Exception e)
//            {
//                System.out.println ("\n Exception: getWinnersMap. e: " + e + "\n");
//            }
//        }
//
//        return winnersMap;
//    }

    private HashMap<Integer, HashMap<String, HashMap<String, Object>>> getWinnersMap (int totalRanks, Map<String, HashMap<String, Object>> playersMap)
    {
        System.out.printf ("Debug: getWinnersMap. totalRanks: %s \n", totalRanks);

        HashMap<Integer, HashMap<String, HashMap<String, Object>>> winnersMap = new HashMap<> ();

        int currentRank = 0;
        int playerPoints = 0;
        int prevPlayerPoints = 0;

        for (Map.Entry<String, HashMap<String, Object>> player : playersMap.entrySet ())
        {
            System.out.printf ("Debug: player key: %s playerValueScore: %s \n", player.getKey (), player.getValue ().get(Constant.PlayerKeys.POINTS_SCORED));

            System.out.printf ("Debug: player. current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);
            try
            {
                HashMap<String, HashMap<String, Object>> tempPlayerMap = new HashMap<> ();
                tempPlayerMap.put (player.getKey (), player.getValue ());

                if (player.getValue ().containsKey (Constant.PlayerKeys.POINTS_SCORED))
                    playerPoints = ConversionHelper.stringToInt (player.getValue ().get(Constant.PlayerKeys.POINTS_SCORED).toString ());

                System.out.printf ("Debug: getWinnersMap. current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);

                if (playerPoints == prevPlayerPoints && winnersMap.containsKey (currentRank))
                {
                    System.out.printf ("Debug: getWinnersMap. Players has same points. playerPoints: %s prevPlayerPoints: %s \n", playerPoints, prevPlayerPoints);
                    winnersMap.get (currentRank).put (player.getKey (), player.getValue ());
                }
                else if (playerPoints < prevPlayerPoints && winnersMap.size () == totalRanks)
                {
                    System.out.printf ("Debug: getWinnersMap. ranks completed current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);
                    break;
                }
                else
                {
                    currentRank += 1;
                    winnersMap.put (currentRank, tempPlayerMap);
                    System.out.printf ("Debug: getWinnersMap. update rank current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);
                }

                //                System.out.printf ("Debug: getWinnersMap. currentRank: %s playerPoints: %s prevPlayerPoints: %s rankDictSize: %s winnersDictSize: %s \n", currentRank, playerPoints, prevPlayerPoints, winnersMap.get (currentRank).size (), winnersMap.size ());
                prevPlayerPoints = playerPoints;
            }
            catch (Exception e)
            {
                System.out.println ("\n Exception: getWinnersMap. e: " + e + "\n");
            }
        }

        return winnersMap;
    }

    private double getTotalRankPercentage (int distributedRank, int totalRanks, int winnersSize, float multiplier)
    {
        System.out.printf ("Debug: getTotalRankPerc. distributeRanks: %s totalRanks: %s winnersSize: %s multiplier: %s \n", distributedRank, totalRanks, winnersSize, multiplier);
        double totalRankPercentage = 0f;

        for (int i = 1; i <= winnersSize; i ++)
        {
            int rank = distributedRank + i;
            // Percentage dedicated to this rank
            double rankPercentage = (Math.pow (multiplier, (totalRanks - rank)) / ConversionHelper.sumOfAllPowers (multiplier, totalRanks));

            totalRankPercentage += rankPercentage;

            System.out.printf ("Debug: getTotalRank. rank: %s distributeRanks: %s totalRankPercentage: %s \n", rank, distributedRank, totalRankPercentage);
        }

        return totalRankPercentage;
    }

    /**
     * distributePrize
     * Distribute the prize between top tier player defined in the game settings
     */
    private void distributePrize (int totalRanks, float multiplier, DataSnapshot playersSnapshot, int gamePrize, FirebaseMapCallback firebaseMapCallback)
    {
        // Convert snapshot to map and inverse
        Map<String, HashMap<String, Object>> playersDataMap = new LinkedHashMap<> ();
        for (DataSnapshot playerSnapshot : playersSnapshot.getChildren ())
        {
            PlayerData playerGameData = new PlayerData ();
            playerGameData.setPlayerId (playerSnapshot.getKey ());
            if (playerSnapshot.hasChild ("name"))
                playerGameData.setPlayerName (playerSnapshot.child ("name").getValue ().toString ());
            if (playerSnapshot.hasChild ("pointsScored"))
                playerGameData.setPointsScored (ConversionHelper.stringToInt (playerSnapshot.child ("pointsScored").getValue ().toString ()));
            if (playerSnapshot.hasChild ("imageName"))
                playerGameData.setPlayerImageName (playerSnapshot.child ("imageName").getValue ().toString ());
            if (playerSnapshot.hasChild ("userType"))
                playerGameData.setUserType (ConversionHelper.stringToInt (playerSnapshot.child ("userType").getValue ().toString ()));

            playerGameData.setLastMatchEarned (0);

            HashMap<String, Object> playerMap = new HashMap<> ();
            playerMap.put ("name", playerGameData.getPlayerName ());
            playerMap.put ("pointsScored", playerGameData.getPointsScored ());
            playerMap.put ("imageName", playerGameData.getPlayerImageName ());
            playerMap.put ("lastMatchEarned", playerGameData.getLastMatchEarned ());
            playerMap.put ("userType", playerGameData.getUserType ());

            playersDataMap.put (playerGameData.getPlayerId (), playerMap);
            System.out.printf ("Debug: Making dictionary points Score. points: %s  \n", playerGameData.getPointsScored ());
        }

        Map<String, HashMap<String, Object>> inversedPlayerDataMap = ConversionHelper.invertMapUsingList (playersDataMap);
        System.out.printf ("Debug: distributePrize top tier. totalRanks: %s multiplier: %s gamePrize: %s \n", totalRanks, multiplier, gamePrize);

        Map<Integer, HashMap<String, HashMap<String, Object>>> totalWinnersMap = getWinnersMap (totalRanks, inversedPlayerDataMap);
        Map<String, HashMap<String, Object>> winnersMap = new HashMap<> ();

        double distributedPercentage = 0;
        double maxPercentage = 1;
        int distributedRanks = 0;
        for (Map.Entry<Integer, HashMap<String, HashMap<String, Object>>> entry : totalWinnersMap.entrySet ())
        {
            Integer currentRank = entry.getKey ();
            HashMap<String, HashMap<String, Object>> winners = entry.getValue ();
            System.out.printf ("Debug: distributePrize top tier. currentRank: %s winners size: %s sumOfAllPowers: %s rankPercentage: %s \n", currentRank, winners.size (), ConversionHelper.sumOfAllPowers (multiplier, totalRanks), Math.pow (multiplier, (totalRanks - currentRank)));

            // Increase in rank percentage if there are more winners at the same spot
            double totalRankPercentage = getTotalRankPercentage (distributedRanks, totalRanks, winners.size (), multiplier);
            // Calculate the remaining rank percentage
            double remianingPercentage = (maxPercentage - distributedPercentage);
            // Set total-rank-perc to remaining percentage if it exceeds the remaining
            if (totalRankPercentage > remianingPercentage)
                totalRankPercentage = remianingPercentage;
            // Set percentage-per-player
            double percentagePerPlayer = totalRankPercentage / winners.size ();
            int prizePerPlayer = (int) Math.floor (gamePrize * (percentagePerPlayer));

            System.out.printf ("Debug: distributePrize-top. distributedPerc: %s maxPerc: %s totalRankPerc: %s remainingPerc: %s PPP: %s PP: %s \n", distributedPercentage, maxPercentage, totalRankPercentage, remianingPercentage, percentagePerPlayer, prizePerPlayer);

            for (Map.Entry<String, HashMap<String, Object>> e : winners.entrySet ())
            {
                String k = e.getKey ();
                HashMap<String, Object> v = e.getValue ();
                PlayerData playerGameData = new PlayerData ();
                playerGameData.setPlayerId (k);
                if (v.containsKey (Constant.PlayerKeys.NAME))
                    playerGameData.setPlayerName (v.get (Constant.PlayerKeys.NAME).toString ());
                if (v.containsKey (Constant.PlayerKeys.POINTS_SCORED))
                    playerGameData.setPointsScored (ConversionHelper.stringToInt (v.get (Constant.PlayerKeys.POINTS_SCORED).toString ()));
                if (v.containsKey (Constant.PlayerKeys.IMAGE_NAME))
                    playerGameData.setPlayerImageName (v.get (Constant.PlayerKeys.IMAGE_NAME).toString ());
                if (v.containsKey (Constant.PlayerKeys.USER_TYPE))
                    playerGameData.setUserType (ConversionHelper.stringToInt (v.get (Constant.PlayerKeys.USER_TYPE).toString ()));

                playerGameData.setLastMatchEarned (prizePerPlayer);

                HashMap<String, Object> playerMap = new HashMap<> ();
                playerMap.put ("name", playerGameData.getPlayerName ());
                playerMap.put ("pointsScored", playerGameData.getPointsScored ());
                playerMap.put ("imageName", playerGameData.getPlayerImageName ());
                playerMap.put ("lastMatchEarned", playerGameData.getLastMatchEarned ());
                playerMap.put ("userType", playerGameData.getUserType ());

                winnersMap.put (k, playerMap);

                System.out.printf ("Debug: winners foreach. id: %s score: %s rank: %s \n", k, playerGameData.getPointsScored (), currentRank);
                distributedPercentage += percentagePerPlayer;
            }
            // Update the distributed ranks
            distributedRanks += winners.size ();
        }


        inversedPlayerDataMap.putAll (winnersMap);

        Map<String, Object> dbMap = ConversionHelper.flatternMap(inversedPlayerDataMap);
        // Update database
        MainApplication.currentGamePlayersDbRef.updateChildren (dbMap, (databaseError, databaseReference) ->
        {
            if (databaseError != null)
            {
                System.out.println ("Data could not be saved " + databaseError.getMessage ());
                LogToFile.logGameExceptions ("DatabaseError: distributePrize() while updating player record. error: " + databaseError);
            }

            System.out.println ("distributed " + databaseReference);
            firebaseMapCallback.onCallback (dbMap);
        });
    }

    /**
     * distributePrize
     * Distribute the prize between all the players who crossed the threshold points
     * @param totalScore => get from calculateTotalPoints ()
     * @param playersSnapshot
     * @param thresholdPoints
     * @param gamePrize
     * @param firebaseMapCallback
     */
    private void distributePrize (int totalScore, DataSnapshot playersSnapshot, int thresholdPoints, int gamePrize, FirebaseMapCallback firebaseMapCallback)
    {
        try
        {
            HashMap<String, Object> playersDataMap = new HashMap<String, Object> ();

            for (DataSnapshot playerSnapshot : playersSnapshot.getChildren ())
            {
                PlayerData playerGameData = new PlayerData ();
                playerGameData.setPlayerId (playerSnapshot.getKey ());
                if (playerSnapshot.hasChild ("name"))
                    playerGameData.setPlayerName (playerSnapshot.child ("name").getValue ().toString ());
                if (playerSnapshot.hasChild ("pointsScored"))
                    playerGameData.setPointsScored (ConversionHelper.stringToInt (playerSnapshot.child ("pointsScored").getValue ().toString ()));
                if (playerSnapshot.hasChild ("imageName"))
                    playerGameData.setPlayerImageName (playerSnapshot.child ("imageName").getValue ().toString ());
                if (playerSnapshot.hasChild ("userType"))
                    playerGameData.setUserType (ConversionHelper.stringToInt (playerSnapshot.child ("userType").getValue ().toString ()));

                if (playerGameData.getPointsScored () >= thresholdPoints)
                {
                    playerGameData.setLastMatchEarned (getPlayerPrize (playerGameData.getPointsScored (), totalScore, gamePrize));
                }
                else
                {
                    playerGameData.setLastMatchEarned (0);
                }

                HashMap<String, Object> playerMap = new HashMap<String, Object> ();
                playerMap.put ("name", playerGameData.getPlayerName ());
                playerMap.put ("pointsScored", playerGameData.getPointsScored ());
                playerMap.put ("imageName", playerGameData.getPlayerImageName ());
                playerMap.put ("lastMatchEarned", playerGameData.getLastMatchEarned ());
                playerMap.put ("userType", playerGameData.getUserType ());

                playersDataMap.put (playerGameData.getPlayerId (), playerMap);
            }

            MainApplication.currentGamePlayersDbRef.updateChildren (playersDataMap, (databaseError, databaseReference) ->
            {
                if (databaseError != null)
                {
                    System.out.println ("Data could not be saved " + databaseError.getMessage ());
                    LogToFile.logGameExceptions ("DatabaseError: distributePrize() while updating player record. error: " + databaseError);
                }

                System.out.println ("distributed " + databaseReference);
                firebaseMapCallback.onCallback (playersDataMap);
            });
        }
        catch (Exception e)
        {
            LogToFile.logGameExceptions ("Exception: distributePrize. exception: " + e);
        }
    }

    /**
     * distributePrize across top 3 players of the game
     * @param playersSnapshot
     * @param gamePrizeSnapshot
     * @param firebaseMapCallback
     */
    private void distributePrize (DataSnapshot playersSnapshot, DataSnapshot gamePrizeSnapshot, FirebaseMapCallback firebaseMapCallback)
    {
        HashMap<String, Object> playersDataMap = new HashMap<String, Object>();
        short prizeKey;

        // Assign the correct prize if total players are <= 3
        if (playersSnapshot.getChildrenCount () == 3)
            prizeKey = 2;
        else if (playersSnapshot.getChildrenCount () == 2)
            prizeKey = 1;
        else
            prizeKey = 0;

        for (DataSnapshot playerSnapshot : playersSnapshot.getChildren())
        {
            PlayerData playerGameData = new PlayerData();
            playerGameData.setPlayerId (playerSnapshot.getKey());
            if (playerSnapshot.hasChild ("name"))
                playerGameData.setPlayerName (playerSnapshot.child ("name").getValue().toString());
            if (playerSnapshot.hasChild ("imageName"))
                playerGameData.setPlayerImageName (playerSnapshot.child ("imageName").getValue().toString());
            if (playerSnapshot.hasChild ("pointsScored"))
                playerGameData.setPointsScored (ConversionHelper.stringToInt (playerSnapshot.child ("pointsScored").getValue().toString()));
            if (playerSnapshot.hasChild ("userType"))
                playerGameData.setUserType (ConversionHelper.stringToInt (playerSnapshot.child ("userType").getValue ().toString ()));

            playerGameData.setPrizeWonSnapshot (gamePrizeSnapshot.child (ConversionHelper.intToString (prizeKey)).getValue ());

            HashMap<String, Object> playerMap = new HashMap<String, Object>();
            playerMap.put ("name", playerGameData.getPlayerName());
            playerMap.put ("imageName", playerGameData.getPlayerImageName ());
            playerMap.put ("pointsScored", playerGameData.getPointsScored());
            playerMap.put ("userType", playerGameData.getUserType ());
            playerMap.put ("prizeWon", playerGameData.getPrizeWonSnapshot ());

            playersDataMap.put (playerGameData.getPlayerId(), playerMap);

            prizeKey--;
        }

        MainApplication.currentGamePlayersDbRef.updateChildren (playersDataMap, new DatabaseReference.CompletionListener()
        {
            @Override
            public void onComplete (DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null)
                {
                    System.out.println("Data could not be saved " + databaseError.getMessage());
                }
                else
                {
                    firebaseMapCallback.onCallback (playersDataMap);
                    System.out.println("distributed "+ databaseReference);
                }
            }
        });
    }

    /**
     * fetchTopThree
     * Fetches the top three players of the game and reward them accordingly
     */
    private void fetchTopThree ()
    {
        MainApplication.currentGamePlayersDbRef.orderByChild ("pointsScored").limitToLast (3).addListenerForSingleValueEvent (new ValueEventListener ()
        {
            @Override
            public void onDataChange (DataSnapshot playersSnapshot)
            {
                distributePrize (playersSnapshot, gameData.getGamePrizeSnapshot (), playersDataMap ->
                {
                    System.out.println ("updating leaderboards");

                    // update calculating results status
                    gameData.setCalculatingResultsStatus (false);
                    HashMap<String, Object> mutableDataMap = new HashMap<String, Object> ();
                    mutableDataMap.put ("isCalculatingResults", gameData.getCalculatingResultsStatus ());
                    updateMutableData (mutableDataMap, null);

                    // Update Leaderboard
                    leaderboardController.processLeaderbaord (gameData.getQuizKey (), gameData.getGamePasswrodProtectedStatus (), gameData.getProductionStatus (), () ->
                    {
                        // Update Played Games Db
                        updatePlayedGames (gameData.getQuizKey (), gameData.getGameImmutableData (), playersDataMap);
                        // Remove the previous game from Db
                        removePreviousGame (gameData.getQuizKey ());
                    });
                });
            }

            @Override
            public void onCancelled (DatabaseError error)
            {

            }
        });
    }

    private int getPlayerPrize (int playerPoints, int allPlayerPoints, int gamePrize)
    {
        return (int) Math.floor (((double) playerPoints / (double) allPlayerPoints) * gamePrize);
    }

    /**
     * updateMutableData
     *
     * @param dataMap
     * @param firebaseMapCallback
     */
    private void updateMutableData (HashMap<String, Object> dataMap, FirebaseMapCallback firebaseMapCallback)
    {
        MainApplication.currentGameDbRef.child ("gameMutableData").updateChildren (dataMap, (databaseError, databaseReference) ->
        {
            if (databaseError != null)
            {
                System.out.println("Data could not be saved " + databaseError.getMessage());
                return;
            }

            if (firebaseMapCallback != null)
                firebaseMapCallback.onCallback (dataMap);

        });
    }

    /**
     * updatePlayedGames
     * Update the playedGames db-ref in db, saves the data of last game
     * @param gameKey
     * @param gameData
     * @param playersData
     */
    private void updatePlayedGames (String gameKey, Object gameData, Object playersData)
    {
        System.out.printf ("updatePlayedGames () %s \n ", gameKey );
        HashMap<String, Object> playedGameMap = new HashMap<> ();
        playedGameMap.put ("gameImmutableData", gameData);
        playedGameMap.put ("gameLeaderboard",  playersData);

        if (this.gameData.getProductionStatus ())
        {
            MainApplication.playedGamesDbRef.child (gameKey).updateChildren (playedGameMap, (error, ref) ->
            {
                System.out.println ("Game has been added on played games " + error);
            });

            // Update played game key and used questions
            MainApplication.playedGamekeysDbRef.child (gameKey).setValueAsync (this.gameData.getQuestionsSnapshot ().getChildrenCount ());
        }
        else
        {
            MainApplication.playedGamesDebugDbRef.child (gameKey).updateChildren (playedGameMap, (error, ref) ->
            {
                if (error != null)
                {
                    System.out.println ("Exception: Db error while updating playedGamesDebug. error "+error);
                    return;
                }

                System.out.println ("Debug Game has been added on played games");
            });

            // Update debug played game keys and used questions
            MainApplication.playedGameDebugkeysDbRef.child (gameKey).setValueAsync (this.gameData.getQuestionsSnapshot ().getChildrenCount ());
        }
    }

    /**
     * removePreviousGame
     * Deletes the quiz from nextGames Db-ref
     * @param gameKey
     */
    private void removePreviousGame (String gameKey)
    {
        System.out.println ("removePreviousGame () " + gameKey);
        MainApplication.nextGamesDbRef.child (gameKey).removeValue ((error, ref) ->
        {
            if (error != null)
            {
                System.out.println("Data could not be saved " + error.getMessage());
            } else
            {
                System.out.println ("Game has been deleted from next games");
            }
        });
    }


}
