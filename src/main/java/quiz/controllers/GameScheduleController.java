package quiz.controllers;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import quiz.data.GameConfigData;
import quiz.data.NextGameData;
import quiz.helpers.Constant;
import quiz.helpers.ConversionHelper;
import quiz.MainApplication;

import java.util.HashMap;
import java.util.Map;

public class GameScheduleController
{
    // Data classes
    private GameConfigData gameConfigData;
    private NextGameData nextGameData;

    // Controllers
    private LeaderboardController leaderboardController;
    private GameController gameController;

    //
    private boolean isGameConfigInitialized;

    public GameScheduleController ()
    {
        gameConfigData = new GameConfigData ();
        // Instantiate GameController and get the instance of LeaderboardController
        gameController = new GameController();
        leaderboardController = LeaderboardController.getInstance();

        MainApplication.gameConfigDbRef.addValueEventListener (new ValueEventListener ()
        {
            @Override
            public void onDataChange (DataSnapshot snapshot)
            {
                updateGameConfig (snapshot);
            }

            @Override
            public void onCancelled (DatabaseError error)
            {

            }
        });

        MainApplication.nextGamesDbRef.addValueEventListener (new ValueEventListener ()
        {
            @Override
            public void onDataChange (DataSnapshot snapshot)
            {
                // Find the earliest game considering current time and save it to Db
                findNextGame (snapshot);
            }

            @Override
            public void onCancelled (DatabaseError error)
            {
                System.out.println (error.toString ());
                System.out.println ("3");
            }
        });
    }

    private void resetGameControllerRef ()
    {
//        if (nextGameData.getQuizKey ().isEmpty ())
//        {
//            System.out.println ("resetGameControllerRef () null");
//            gameController = null;
//            return;
//        }
        // Initialize the game mutable data and update the data
        nextGameData.initializeMutableData();

        gameController.resetReferences (nextGameData, leaderboardController, gameConfigData);
    }

    private void findNextGame (DataSnapshot nextGamesSnapshot)
    {
        nextGameData = new NextGameData ();

        boolean isProduction;
        long gameStartTime = 0, prevGameTimeDiff = 0, nextGameTimeDiff = 0;
        long prodGameStartTime = 0, prodPrevGameTimeDiff = 0, prodNextGameTimeDiff = 0;
        String gameKey = "", prodGameKey = "";

        for (DataSnapshot gameSnapshot : nextGamesSnapshot.getChildren())
        {
            try
            {
                isProduction = ConversionHelper.stringToBool (gameSnapshot.child ("isProduction").getValue ().toString ());
                if (isProduction)
                {
                    prodGameStartTime = ConversionHelper.stringToLong (gameSnapshot.child ("startTime").getValue ().toString ());
                    // Calculate the differences
                    prodNextGameTimeDiff = prodGameStartTime - TimerController.getCurrentTime ();

                    // For the first difference
                    if (prodPrevGameTimeDiff == 0 && prodNextGameTimeDiff > 0)
                        prodPrevGameTimeDiff = prodNextGameTimeDiff;

                    // If the game start-time is greater than the current-time, set this game as a next game
                    if (prodNextGameTimeDiff > 0 && prodNextGameTimeDiff <= prodPrevGameTimeDiff)
                    {
                        // Assign new value to Prev game-time difference amd gameKey
                        prodPrevGameTimeDiff = prodNextGameTimeDiff;
                        prodGameKey = gameSnapshot.getKey ();
                    }
                }

                gameStartTime = ConversionHelper.stringToLong (gameSnapshot.child ("startTime").getValue ().toString ());
                // Calculate the differences
                nextGameTimeDiff = gameStartTime - TimerController.getCurrentTime ();

                if (prevGameTimeDiff == 0 && nextGameTimeDiff > 0)
                    prevGameTimeDiff = nextGameTimeDiff;

                // If the game start-time is greater than the current-time, set this game as a next game
                if (nextGameTimeDiff > 0 && nextGameTimeDiff <= prevGameTimeDiff)
                {
                    // Assign new value to Prev game-time difference amd gameKey
                    prevGameTimeDiff = nextGameTimeDiff;
                    gameKey = gameSnapshot.getKey ();
                }
            }
            catch (Exception e)
            {
                System.out.println ("FirebaseException: "+e);
            }
        }

        try
        {
            Map<String, Object> gameDataMap = new HashMap <String, Object>();
            if (!gameKey.isEmpty ())
            {
                // Get the key of this quiz
                nextGameData.setQuizKey (gameKey);
                nextGameData.setQuizTypeSnapshot (nextGamesSnapshot.child (gameKey).child ("quizType"));
                // Get the data from snapshot and set it to NextGameData class
                nextGameData.setTotalQuestions (ConversionHelper.stringToInt (nextGamesSnapshot.child (gameKey).child ("totalQuestions").getValue ().toString ()));
                nextGameData.setQuestionTime (ConversionHelper.stringToInt (nextGamesSnapshot.child (gameKey).child ("questionTime").getValue ().toString ()));
                nextGameData.setAnswerTime (ConversionHelper.stringToInt (nextGamesSnapshot.child (gameKey).child (Constant.GameKeys.ANSWER_TIME).getValue ().toString ()));
                nextGameData.setQuestionChangeTime (ConversionHelper.stringToInt (nextGamesSnapshot.child (gameKey).child (Constant.GameKeys.QUESTION_CHANGE_TIME).getValue ().toString ()));
                if (nextGamesSnapshot.child (gameKey).hasChild ("thresholdPoints") && ConversionHelper.stringToInt (nextGameData.getQuizTypeSnapshot ().child ("distributionType").getValue ().toString ()) != Constant.GameplaySettings.DISTRIBUTE_TO_RANKS)
                     nextGameData.setThresholdPoints (ConversionHelper.stringToInt (nextGamesSnapshot.child (gameKey).child ("thresholdPoints").getValue ().toString ()));

                nextGameData.setQuizNotificationTime (ConversionHelper.secToMillisec (ConversionHelper.stringToInt (nextGamesSnapshot.child (gameKey).child ("quizNotificationTime").getValue ().toString ())));
                nextGameData.setQuizNotificationTitle (nextGamesSnapshot.child (gameKey).child ("quizNotificationTitle").getValue ().toString ());
                nextGameData.setQuizNotificationMessage (nextGamesSnapshot.child (gameKey).child ("quizNotificationMessage").getValue ().toString ());

//                nextGameData.setDefaultNotificationTitle (nextGamesSnapshot.child (gameKey).child ("defaultNotificationTitle").getValue ().toString ());
//                nextGameData.setDefaultNotificationMessage (nextGamesSnapshot.child (gameKey).child ("defaultNotificationMessage").getValue ().toString ());

                nextGameData.setGamePasswordProtectedStatus (ConversionHelper.stringToBool (nextGamesSnapshot.child (gameKey).child ("isPasswordProtected").getValue ().toString ()));
                nextGameData.setProductionStatus (ConversionHelper.stringToBool (nextGamesSnapshot.child (gameKey).child ("isProduction").getValue ().toString ()));
                nextGameData.setGameImmutableData (nextGamesSnapshot.child (gameKey).getValue ()); // Set the game data in NextGame Immutable Data
                if (!nextGameData.getProductionStatus ())
                {
                    if (!prodGameKey.isEmpty ())
                        nextGameData.setProdGameData (nextGamesSnapshot.child (prodGameKey).getValue ());
                }
                nextGameData.setQuestionsSnapshot (nextGamesSnapshot.child (gameKey).child ("quiz"));
                if (ConversionHelper.stringToInt (nextGameData.getQuizTypeSnapshot ().child ("distributionType").getValue ().toString ()) == 0 || ConversionHelper.stringToInt (nextGameData.getQuizTypeSnapshot ().child ("distributionType").getValue ().toString ()) == 2)
                {
                    nextGameData.setGamePrize (ConversionHelper.stringToInt (nextGamesSnapshot.child (gameKey).child ("gamePrice").getValue ().toString ()));
                }
                else
                {
                    nextGameData.setGamePrizeSnapshot (nextGamesSnapshot.child (gameKey).child ("gamePrice"));
                }

                if (nextGamesSnapshot.child (gameKey).hasChild (Constant.GameKeys.MULTIPLIER))
                    nextGameData.setMultiplier (ConversionHelper.stringToFloat (nextGamesSnapshot.child (gameKey).child (Constant.GameKeys.MULTIPLIER).getValue ().toString ()));
                if (nextGamesSnapshot.child (gameKey).hasChild (Constant.GameKeys.RANKS))
                    nextGameData.setRanks (ConversionHelper.stringToInt (nextGamesSnapshot.child (gameKey).child (Constant.GameKeys.RANKS).getValue ().toString ()));

                nextGameData.setStartTime (ConversionHelper.stringToLong (nextGamesSnapshot.child (gameKey).child ("startTime").getValue ().toString ()));
                nextGameData.setEndTime (getGameEndTime (nextGameData.getStartTime (), nextGameData.getQuestionTime (), nextGameData.getTotalQuestions (), nextGameData.getAnswerTime (), nextGameData.getQuestionChangeTime ()));

                // Calculate end time for quiz
                for (DataSnapshot question : nextGamesSnapshot.child (gameKey).child ("quiz").getChildren ())
                {
                    if (question.hasChild ("ad"))
                    {
                        nextGameData.setEndTime (nextGameData.getEndTime () + (ConversionHelper.stringToLong (question.child ("ad").child ("adDuration").getValue ().toString ()) * 1000));
                    }
                }


                gameDataMap.put ("prodGame", nextGameData.getProdGameData ());
                gameDataMap.put ("quizKey", nextGameData.getQuizKey());
//                gameDataMap.put ("game", nextGameData.getGameImmutableData());
                gameDataMap.put ("gameImmutableData", nextGameData.getGameImmutableData());
            }
            else
            {
                // Get the key of this quiz
                nextGameData.setQuizKey (gameKey);
                gameDataMap.put ("quizKey", "");
            }

            saveNextGame (gameDataMap);
        }
        catch (Exception e)
        {
            System.out.println ("Exception: findNextGame () "+ e.toString () + " game key "+ nextGameData.getQuizKey ());
        }
    }

    private long getGameEndTime (long startTime, long questionTime, long noOfQuestions, long answerTime, long questionChangeTime)
    {
//        endTime: action.payload + Object.keys(state.nextGame.quiz).length * ((parseInt (state.nextGame.answerTime) + parseInt (state.nextGame.questionTime) + state.nextGame.questionChangeTime) * 1000), // Converting time into milliseconds
        return startTime + ( noOfQuestions * ( ( questionTime + answerTime + questionChangeTime ) * 1000 ) );
    }

    /**
     * Save the game in Db
     */
    private void saveNextGame (Map gameDataMap)
    {
        MainApplication.currentGameDbRef.setValue (gameDataMap, (databaseError, databaseReference) ->
        {
            if (databaseError != null)
            {
                System.out.println ("Firebase:error Data could not be saved " + databaseError.getMessage());
            }
            else
            {
                System.out.println ("Firebase:success saveNextGame ()");
            }
        });

        // Reset/Pass new GameData to GameController
        resetGameControllerRef ();
    }


    private void updateGameConfig (DataSnapshot configDataSnapshot)
    {
        System.out.println ("Debug: update game config");
        this.gameConfigData = new GameConfigData ();

//        if (configDataSnapshot.hasChild (Constant.ConfigKeys.IS_PRODUCTION))
//            gameConfigData.setProductionStatus (ConversionHelper.stringToBool (configDataSnapshot.child (Constant.ConfigKeys.IS_PRODUCTION).getValue ().toString ()));
        if (configDataSnapshot.hasChild (Constant.ConfigKeys.NOTIFICATION_TIME))
            gameConfigData.setNotificationTime (ConversionHelper.secToMillisec (ConversionHelper.stringToLong (configDataSnapshot.child (Constant.ConfigKeys.NOTIFICATION_TIME).getValue ().toString ())));
        if (configDataSnapshot.hasChild (Constant.ConfigKeys.NOTIFICATION_TITLE))
            gameConfigData.setNotificationTitle (configDataSnapshot.child (Constant.ConfigKeys.NOTIFICATION_TITLE).getValue ().toString ());
        if (configDataSnapshot.hasChild (Constant.ConfigKeys.NOTIFICATION_MESSAGE))
            gameConfigData.setNotificationMessage (configDataSnapshot.child (Constant.ConfigKeys.NOTIFICATION_MESSAGE).getValue ().toString ());

        System.out.println ("production status "+ gameConfigData.getProductionStatus ());
    }
}
