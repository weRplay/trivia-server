package quiz.controllers;


import java.io.BufferedReader;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.*;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import org.json.JSONObject;
import quiz.config.GameConfig;
import quiz.helpers.LogToFile;

public class PushNotificationController
{

    public final static String URL = "https://fcm.googleapis.com/fcm/send";
    public final static String SERVER_KEY = "AAAAhdFv00Q:APA91bFXZTZEPflomh52RYyt6Zgm8IhN-htwjBWUtl3JfhOeYltoWz-estNSl_rONMRJ19fQ9YrXwzUlpyHGy7cuZRIRHTez_nH8M8Oov5CfQdw5eNoC5x_MbELvhvioMqUmrf8Ff_3K";

    private static String topic = "general";

    public static void sendPushNotification (String title, String message, boolean isProduction)
    {
//        try {
//            if (!isProduction)
//                topic = "/topics/debug";
//
//            URL url = new URL(URL);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//
//            conn.setUseCaches(false);
//            conn.setDoInput(true);
//            conn.setDoOutput(true);
//
//            conn.setRequestMethod("POST");
//
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestProperty("Authorization", "key=" + SERVER_KEY);
//
//            // System.out.println(root.toString());
//
//            // Initializing json object for sending it as a data
//            JSONObject json = new JSONObject();
//            json.put("to", topic);
//
//            // Json object for notification-message title and text
//            JSONObject info = new JSONObject();
//            info.put("title", title); // Notification title
//            info.put("body", message); // Notification text
//
//            json.put("notification", info);
//
//            OutputStreamWriter wr = new OutputStreamWriter (conn.getOutputStream());
//            wr.write (json.toString());
//            wr.flush ();
//
//            BufferedReader br = new BufferedReader (new InputStreamReader((conn.getInputStream())));
//
//            String output;
//            StringBuilder builder = new StringBuilder ();
//
//            while ((output = br.readLine()) != null)
//            {
//                builder.append(output);
//            }
//
//            System.out.println (builder);
//            String result = builder.toString();
//
//            JSONObject obj = new JSONObject(result);
//
//            System.out.println ("Update log file");
//            LogToFile.logPushNotificationStatus (builder.toString ());
//            br.close ();
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace ();
//            System.out.println ("error "+ e);
//        }
        try
        {
            System.out.println ("Debug: sendPushNotification title : message : " + title + " : " + message);
            if (!isProduction)
                topic = "debug";

            Message messageBuilder = Message.builder ()
                    .setNotification (new Notification (
                            title,
                            message
                    ))
                    .setTopic (topic)
                    .build ();

            String response = FirebaseMessaging.getInstance ().send (messageBuilder);
            LogToFile.logPushNotificationStatus (response);

            System.out.println ("Response: sendPushNotification, r. " + response);
        }
        catch (Exception e)
        {
            System.out.println ("Exception: sendPushNotification, e. " + e);
            LogToFile.logPushNotificationStatus ("Exception: sendPushNotification(). e: " + e);
        }
    }

}
