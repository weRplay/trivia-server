package quiz.controllers;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import quiz.MainApplication;
import quiz.data.PlayerData;
import quiz.helpers.ConversionHelper;

import javax.xml.crypto.Data;
import java.util.HashMap;
import java.util.Map;

public class CashoutController
{
//    private PlayerData playerData;
//
//    /**
//     * CashoutController
//     * Process the Easypaisa functionality whenever the user tree gets updated
//     */
//    public CashoutController ()
//    {
//        MainApplication.usersDbRef.addChildEventListener (new ChildEventListener ()
//        {
//            @Override
//            public void onChildAdded (DataSnapshot snapshot, String previousChildName)
//            {
//
//            }
//
//            @Override
//            public void onChildChanged (DataSnapshot playerSnapshot, String previousChildName)
//            {
//                System.out.println ("CahsoutController ()"+ previousChildName);
//                if (checkPlayerCahshoutStatus (playerSnapshot))
//                {
//                    deductPlayerMoney (playerSnapshot);
//                }
//            }
//
//            @Override
//            public void onChildRemoved (DataSnapshot snapshot)
//            {
//
//            }
//
//            @Override
//            public void onChildMoved (DataSnapshot snapshot, String previousChildName)
//            {
//
//            }
//
//            @Override
//            public void onCancelled (DatabaseError error)
//            {
//
//            }
//        });
//    }
//
//    /**
//     * checkPlayerCashoutStatus
//     * Validates that the user has submit transaction request
//     * cashoutStatus: 0 => nothing, 1 => player trying to checkout, 2 => checkout is in process, 3 => Error
//     * userType: 0 => Guest User, 1 => Mobile Number Verified
//     * @param playerSnapshot
//     */
//    private boolean checkPlayerCahshoutStatus (DataSnapshot playerSnapshot)
//    {
//        if (!playerSnapshot.hasChild ("cashoutStatus"))
//            return false;
//
//        int checkoutStatus = ConversionHelper.stringToInt (playerSnapshot.child ("cashoutStatus").getValue ().toString ());
//        int userType = ConversionHelper.stringToInt (playerSnapshot.child ("userType").getValue ().toString ());
//        if (checkoutStatus != 1 || userType != 1)
//            return false;
//
//        return true;
//    }
//
//    /**
//     * deductPlayerMoney
//     * Transfers the total amount of user to easy paisa account and if it succeeds, resets the player totalEarned to 0.
//     * @param playerSnapshot
//     */
//    private void deductPlayerMoney (DataSnapshot playerSnapshot)
//    {
//        playerData = new PlayerData ();
//        playerData.setPlayerId (playerSnapshot.getKey ());
//        playerData.setTotalEarned (ConversionHelper.stringToInt (playerSnapshot.child ("totalEarned").getValue ().toString ()));
//        playerData.setCashoutStatus (ConversionHelper.stringToInt (playerSnapshot.child ("cashoutStatus").getValue ().toString ()));
//        playerData.setMobileNumber (playerSnapshot.child ("mobileNumber").getValue ().toString ());
//
//        int transferMoneyToEasypaisaAccountStatus = transferMoneyToEasypaisaAccount (playerData.getMobileNumber ());
//        if (transferMoneyToEasypaisaAccountStatus == 200)
//        {
//            playerData.setTotalEarned (0);
//
//            Map<String, Object> profileDataMap = new HashMap ();
//            profileDataMap.put ("totalEarned", playerData.getTotalEarned ());
//
//            updatePlayerProfile (playerData.getPlayerId (), profileDataMap);
//        }
//        else if (transferMoneyToEasypaisaAccountStatus == 500)
//        {
//            Map<String, Object> profileDataMap = new HashMap ();
//            profileDataMap.put ("cashoutStatus", 3);
//
//            updatePlayerProfile (playerData.getPlayerId (), profileDataMap);
//        }
//    }
//
//    /**
//     * transferMoneyToEasypaisaAccount
//     * @param mobileNumber => number is used to transfer the money to Easypaisa account
//     * @return => status of transaction
//     */
//    private int transferMoneyToEasypaisaAccount (String mobileNumber)
//    {
//        return 500;
//    }
//
//
//    /**
//     * updatePlayerProfile
//     * updates the user profile according to the EasyPaisa status.
//     * @param playerId
//     * @param playerDataMap
//     */
//    private void updatePlayerProfile (String playerId, Map<String, Object> playerDataMap)
//    {
//        MainApplication.usersDbRef.child (playerId).updateChildren (playerDataMap, (error, ref) ->
//        {
//            if (error != null)
//                System.out.println ("FirebaseError: Error while updating the user profile. updateProfile ()");
//            else
//                System.out.println ("Profile has been updated");
//        });
//    }
}
