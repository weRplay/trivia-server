package quiz.helpers;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.*;
import quiz.MainApplication;
import quiz.controllers.LeaderboardController;

import java.util.*;
import java.util.stream.Collectors;

public class HelperScript
{
    private static String gameId = "-LwVv6VrA3ssMfBxzf-c";

    private static Map<String, Integer> usedNames = new HashMap<> ();

    public static void FixUsersLastGameLeaderboard ()
    {
        DatabaseReference gameDbRef = MainApplication.playedGamesDbRef.child (gameId).child ("gameLeaderboard");
        LeaderboardController.getInstance ().updateSpecificGameLeaderboard (gameDbRef, gameId);
    }

    /**
     * TruncateUsersMobileNumbers
     * used to remove hidden characters from all users mobile number
     */
    public static void TruncateUsersMobileNumbers ()
    {
        System.out.println ("Debug: TruncateUsersMobileNumbers");
        MainApplication.usersDbRef.addValueEventListener (new ValueEventListener()
        {
            @Override
            public void onDataChange (DataSnapshot usersSnap)
            {
                System.out.println ("Debug: update users mobile number");
                for (DataSnapshot user : usersSnap.getChildren ())
                {
                    System.out.print ("key name number "+ user.getKey ());
                }
            }

            @Override
            public void onCancelled (DatabaseError error)
            {
                System.out.println ("Debug: Error on update specific leaderboard "+error);
            }
        });
    }

    public static void MakeUsernamesUnique ()
    {
        try
        {
            MainApplication.usersDbRef.addListenerForSingleValueEvent (new ValueEventListener ()
            {
                @Override
                public void onDataChange (DataSnapshot snapshot)
                {
                    System.out.println ("Debug: update users names. noOfPlayers: "+snapshot.getChildrenCount ());
                    int i = 0;
                    for (DataSnapshot user : snapshot.getChildren ())
                    {
                        System.out.println ("Iteration number "+i);
                        try
                        {
                            if (user.hasChild ("userType") && user.hasChild (Constant.PlayerKeys.NAME))
                            {
                                //                        if (ConversionHelper.stringToInt (user.child ("userType").getValue ().toString ()) == 2) // only for debug users
                                //                        {
                                String newName = FindUserWithName (snapshot, user.getKey ());

                                Map<String, Object> newNameMap = new HashMap<> ();
                                newNameMap.put (Constant.PlayerKeys.IS_NAME_CHANGED, false);
                                newNameMap.put (Constant.PlayerKeys.NAME, newName);

                                Map<String, Object> newNameLeaderboardMap = new HashMap<> ();
                                newNameMap.put (Constant.PlayerKeys.NAME, newName);

                                try
                                {
                                    // Update this name
                                    MainApplication.usersDbRef.child (user.getKey ()).updateChildren (newNameMap, new DatabaseReference.CompletionListener ()
                                    {
                                        @Override
                                        public void onComplete (DatabaseError databaseError, DatabaseReference databaseReference)
                                        {
                                            if (databaseError != null)
                                            {
                                                System.out.println ("Data could not be saved" + databaseError.getMessage ());
                                            }
                                            else
                                            {
                                                System.out.printf ("\n player name updated in users id: %s name: %s newName: %s ", user.getKey (), snapshot.child (user.getKey ()).child (Constant.PlayerKeys.NAME).getValue ().toString (), newName);
                                            }
                                        }
                                    });

                                    MainApplication.leaderboardDbRef.child (user.getKey ()).updateChildren (newNameLeaderboardMap, new DatabaseReference.CompletionListener ()
                                    {
                                        @Override
                                        public void onComplete (DatabaseError databaseError, DatabaseReference databaseReference)
                                        {
                                            if (databaseError != null)
                                            {
                                                System.out.println ("Data could not be saved" + databaseError.getMessage ());
                                            }
                                            else
                                            {
                                                System.out.printf ("\n player name updated in leaderboard id: %s name: %s newName: %s ", user.getKey (), snapshot.child (user.getKey ()).child (Constant.PlayerKeys.NAME).getValue ().toString (), newName);
                                            }
                                        }
                                    });
                                } catch (Exception e)
                                {
                                    System.out.printf ("\n Exception: usernames update %s ", e);
                                }
                                //                        }
                            }

                            Thread.sleep (10);
                        } catch (InterruptedException e)
                        {
                            System.out.println ("Exception: Make usernames unique. "+e);
                        }
                        i++;
                    }
                }

                @Override
                public void onCancelled (DatabaseError error)
                {
                    System.out.printf ("\n Error: player name update. %s ", error);
                }
            });
        }
        catch (Exception e)
        {
            System.out.println ("Exception: Make usernames unique. "+e);
        }
    }

    public static void RemoveEasypaisa ()
    {
        try
        {
            MainApplication.usersDbRef.addListenerForSingleValueEvent (new ValueEventListener ()
            {
                @Override
                public void onDataChange (DataSnapshot snapshot)
                {
                    System.out.println ("Debug: update users names. noOfPlayers: "+snapshot.getChildrenCount ());
                    int i = 0;
                    for (DataSnapshot user : snapshot.getChildren ())
                    {
                        if (user.hasChild ("easypaisaInfo"))
                        {
                            System.out.println ("Iteration number " + i);
                            try
                            {
                                //                            if (ConversionHelper.stringToInt (user.child ("userType").getValue ().toString ()) == 2) // only for debug users
                                //                            {
                                // Update this name
                                MainApplication.usersDbRef.child (user.getKey ()).child ("easypaisaInfo").removeValue (new DatabaseReference.CompletionListener ()
                                {
                                    @Override
                                    public void onComplete (DatabaseError databaseError, DatabaseReference databaseReference)
                                    {
                                        if (databaseError != null)
                                        {
                                            System.out.println ("Data could not be saved" + databaseError.getMessage ());
                                        }
                                        else
                                        {
                                            System.out.printf ("\n player name updated in users id: %s name: %s newName: %s ", user.getKey (), snapshot.child (user.getKey ()).child (Constant.PlayerKeys.NAME).getValue ().toString ());
                                        }
                                    }
                                });
                                //                            }

                                Thread.sleep (5);
                            } catch (InterruptedException e)
                            {
                                System.out.println ("Exception: Make usernames unique. " + e);
                            }
                            i++;
                        }
                    }
                }

                @Override
                public void onCancelled (DatabaseError error)
                {
                    System.out.printf ("\n Error: player name update. %s ", error);
                }
            });
        }
        catch (Exception e)
        {
            System.out.println ("Exception: Make usernames unique. "+e);
        }
    }

    public static void RemoveGuestUsers ()
    {
        try
        {
            MainApplication.usersDbRef.addListenerForSingleValueEvent (new ValueEventListener ()
            {
                @Override
                public void onDataChange (DataSnapshot snapshot)
                {
                    System.out.println ("Debug: update users db. noOfPlayers: "+snapshot.getChildrenCount ());
                    int i = 0;
                    for (DataSnapshot user : snapshot.getChildren ())
                    {
                        int authType;
                        String phoneNumber = "";
                        phoneNumber = (user.hasChild ("phoneNumber") ? user.child ("phoneNumber").getValue ().toString () : "");

                        if (user.hasChild ("userType"))
                            authType = ConversionHelper.stringToInt (user.child ("userType").getValue ().toString ());
                        else if (user.hasChild ("authType"))
                            authType = ConversionHelper.stringToInt (user.child ("authType").getValue ().toString ());
                        else
                            authType = 0;

                        if (authType == 0 && !user.hasChild ("syncedAuth") && phoneNumber.isEmpty ())
                        {
                            System.out.println ("Iteration number " + i);
                            try
                            {
                                // Update this name
                                MainApplication.usersDbRef.child (user.getKey ()).removeValue (new DatabaseReference.CompletionListener ()
                                {
                                    @Override
                                    public void onComplete (DatabaseError databaseError, DatabaseReference databaseReference)
                                    {
                                        if (databaseError != null)
                                        {
                                            System.out.println ("Data could not be saved" + databaseError.getMessage ());
                                        }
                                        else
                                        {
                                            System.out.printf ("\n User Deleted Successfully users id: %s name: %s newName: %s ", user.getKey (), snapshot.child (user.getKey ()).child (Constant.PlayerKeys.NAME).getValue ().toString ());
                                        }
                                    }
                                });

                                Thread.sleep (5);
                            } catch (InterruptedException e)
                            {
                                System.out.println ("Exception: Make usernames unique. " + e);
                            }
                            i++;
                        }
                    }
                }

                @Override
                public void onCancelled (DatabaseError error)
                {
                    System.out.printf ("\n Error: player name update. %s ", error);
                }
            });
        }
        catch (Exception e)
        {
            System.out.println ("Exception: Remove guest users. "+e);
        }
    }

    public static void FindUniqueEasypaisaAccounts ()
    {
        try
        {
            MainApplication.usersDbRef.addListenerForSingleValueEvent (new ValueEventListener ()
            {
                @Override
                public void onDataChange (DataSnapshot snapshot)
                {
                    System.out.println ("Debug: find unique easypaisa accounts. noOfPlayers: "+snapshot.getChildrenCount ());
                    int i = 0;
                    for (DataSnapshot user : snapshot.getChildren ())
                    {
                        System.out.println ("Iteration number "+i);

                        if (user.hasChild ("userType") && user.hasChild (Constant.PlayerKeys.NAME) && user.hasChild ("easypaisaInfo"))
                        {
                            String cnic = ConversionHelper.decodeString (user.child ("easypaisaInfo").child ("cnic").getValue ().toString ());
                            String textTemplate = "userId: " + user.getKey () + " Easypaisa-cnic: " + cnic;

                            if (FindUserWithCnic (user.getKey (), cnic))
                            {
                                textTemplate += " IsDuplicated: True";
                            }
                            else
                            {
                                textTemplate += " IsDuplicated: false";
                            }
                            LogToFile.logUniqueEasypaisaAccounts (textTemplate);
                        }

                        i++;
                    }


                    System.out.println ("Easypaisa Accounts Completed ");
                }

                @Override
                public void onCancelled (DatabaseError error)
                {
                    System.out.printf ("\n Error: player name update. %s ", error);
                }
            });
        }
        catch (Exception e)
        {
            System.out.println ("Exception: Find Unique Easypaisa Accounts. "+e);
        }
    }

    private static Boolean FindUserWithCnic (String keyInSnap, String cnic)
    {
        Boolean returnState;
        int sameCnicCount = 0;

        if (usedNames.containsKey (cnic))
        {
            sameCnicCount = (int) (usedNames.get (cnic)+1);
            returnState = true;
        }
        else
        {
            returnState = false;
        }

        Integer put = usedNames.put (cnic, sameCnicCount);

        return returnState;
    }

    private static String FindUserWithName (DataSnapshot snapshot, String keyInSnap)
    {
        String name = ConversionHelper.removeHiddenCharacters (snapshot.child (keyInSnap).child (Constant.PlayerKeys.NAME).getValue ().toString ());
        String uniqueName;
        int sameNameCount = 0;
        if (usedNames.containsKey (name))
        {
            sameNameCount = (int) (usedNames.get (name)+1);
            uniqueName = name + sameNameCount;
        }
        else
        {
            uniqueName = name;
        }

        Integer put = usedNames.put (name, sameNameCount);
//        return ConversionHelper.removeHiddenCharacters (uniqueName);
        return  uniqueName;
    }

    // Test functions
    public static void TestHashmap ()
    {
        Map<String, HashMap<String, Object>> playersDataMap = new LinkedHashMap<> ();

        for (int i = -4; i < 10; i++)
        {
            HashMap<String, Object> playerMap = new HashMap<> ();
            playerMap.put ("name", "abc"+i);
            playerMap.put ("pointsScored", i * 2);
            playerMap.put ("imageName", "lsdfjks"+i);

            playersDataMap.put ((i == 1) ? "niuypBLyqDepzQibe0YOevtiixa2" : "SgkUObHDrpWcIG8z7Z"+i+"1vsXY1yJV", playerMap);
        }
//        ConversionHelper.invertMapUsingList (playersDataMap);
        Map<String, HashMap<String, Object>> inversedPlayerDataMap = ConversionHelper.invertMapUsingList (playersDataMap);
        Map<Integer, HashMap<String, HashMap<String, Object>>> totalWinnersMap = getWinnersMap (10, inversedPlayerDataMap);

        for (Map.Entry<String, HashMap<String, Object>> player : inversedPlayerDataMap.entrySet ())
        {
            System.out.printf ("Debug: size: %s player key: %s playerValueScore: %s \n", playersDataMap.size (), player.getKey (), player.getValue ().get ("pointsScored"));
        }

        Map<String, Object> flatternMap = new HashMap<> ();
        for (Map.Entry<String, HashMap<String, Object>> player : ConversionHelper.invertMap (playersDataMap).entrySet ())
        {
            flatternMap.put (player.getKey (), player.getValue ());
        }

    }

    private static HashMap<Integer, HashMap<String, HashMap<String, Object>>> getWinnersMap (int totalRanks, Map<String, HashMap<String, Object>> playersMap)
    {
        System.out.printf ("Debug: getWinnersMap. totalRanks: %s \n", totalRanks);

        HashMap<Integer, HashMap<String, HashMap<String, Object>>> winnersMap = new HashMap<> ();

        int currentRank = 0;
        int playerPoints = 0;
        int prevPlayerPoints = 0;

        for (Map.Entry<String, HashMap<String, Object>> player : playersMap.entrySet ())
        {
            System.out.printf ("Debug: player key: %s playerValueScore: %s \n", player.getKey (), player.getValue ().get(Constant.PlayerKeys.POINTS_SCORED));

            System.out.printf ("Debug: player. current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);
            try
            {
                HashMap<String, HashMap<String, Object>> tempPlayerMap = new HashMap<> ();
                tempPlayerMap.put (player.getKey (), player.getValue ());

                if (player.getValue ().containsKey (Constant.PlayerKeys.POINTS_SCORED))
                    playerPoints = ConversionHelper.stringToInt (player.getValue ().get(Constant.PlayerKeys.POINTS_SCORED).toString ());

                System.out.printf ("Debug: getWinnersMap. current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);

                if (playerPoints == prevPlayerPoints && winnersMap.containsKey (currentRank))
                {
                    System.out.printf ("Debug: getWinnersMap. Players has same points. playerPoints: %s prevPlayerPoints: %s \n", playerPoints, prevPlayerPoints);
                    winnersMap.get (currentRank).put (player.getKey (), player.getValue ());
                }
                else if (playerPoints < prevPlayerPoints && winnersMap.size () == totalRanks)
                {
                    System.out.printf ("Debug: getWinnersMap. ranks completed current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);
                    break;
                }
                else
                {
                    currentRank += 1;
                    winnersMap.put (currentRank, tempPlayerMap);
                    System.out.printf ("Debug: getWinnersMap. update rank current rank: %s playerPoints: %s prevPlayerPoints: %s \n", currentRank, playerPoints, prevPlayerPoints);
                }

                //                System.out.printf ("Debug: getWinnersMap. currentRank: %s playerPoints: %s prevPlayerPoints: %s rankDictSize: %s winnersDictSize: %s \n", currentRank, playerPoints, prevPlayerPoints, winnersMap.get (currentRank).size (), winnersMap.size ());
                prevPlayerPoints = playerPoints;
            }
            catch (Exception e)
            {
                System.out.println ("\n Exception: getWinnersMap. e: " + e + "\n");
            }
        }

        return winnersMap;
    }
}

