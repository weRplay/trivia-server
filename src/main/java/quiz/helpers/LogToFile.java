package quiz.helpers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LogToFile
{

    private static String pushNotificationLogFile;
    private static String gameExceptionLogFile;
    private static String logFile;

    public static void logPushNotificationStatus (String exceptionLog)
    {
        pushNotificationLogFile = FilePath.getPushNotificationLogFile ();

        File file;
        FileWriter fileWriter;

        try
        {
            file = new File (pushNotificationLogFile);
            fileWriter = new FileWriter (pushNotificationLogFile, true);

            if (file.createNewFile ())
            {
                fileWriter.write (exceptionLog);
            }
            else
            {
                fileWriter.write ("\n " + exceptionLog);
            }
            fileWriter.close ();
        }
        catch (IOException e)
        {
            System.out.println ("Exception: " + e);
        }
    }

    public static void logGameExceptions (String exceptionLog)
    {
        gameExceptionLogFile = FilePath.getGameExceptionLogFile ();

        File file;
        FileWriter fileWriter;
        BufferedWriter bufferedWriter;

        try
        {
            file = new File (gameExceptionLogFile);
            fileWriter = new FileWriter (gameExceptionLogFile, true);
            bufferedWriter = new BufferedWriter (fileWriter);

            bufferedWriter.write (exceptionLog);
            bufferedWriter.newLine ();
            bufferedWriter.flush ();

            bufferedWriter.close ();
            fileWriter.close ();
        }
        catch (IOException e)
        {
            System.out.println ("Exception: " + e);
        }
    }

    public static void logUniqueEasypaisaAccounts (String logText)
    {
        logFile = FilePath.getCustomLogFile ();

        File file;
        FileWriter fileWriter;

        try
        {
            file = new File (logFile);
            fileWriter = new FileWriter (logFile, true);

            if (file.createNewFile ())
            {
                fileWriter.write (logText);
            }
            else
            {
                fileWriter.write ("\n " + logText);
            }
            fileWriter.close ();
        }
        catch (IOException e)
        {
            System.out.println ("Exception: " + e);
        }
    }
}
