package quiz.helpers;

public class Constant
{
    public static enum DistributionType { DistributeToAll, DistributeToTop3, DistributeToRanks };

    public static class DbNodes
    {
        public static String DB_URL = "https://quiz-be15e.firebaseio.com/";
    }

    public static class GameKeys
    {
        // Game creation keys
        public static String QUESTION_TIME = "questionTime";
        public static String ANSWER_TIME = "answerTime";
        public static String QUESTION_CHANGE_TIME = "questionChangeTime";
        public static String END_TIME = "endTime";
        public static String AD = "ad";
        public static String AD_DURATION = "adDuration";
        public static String DISTRIBUTION_TYPE = "distributionType";
        public static String MULTIPLIER = "multiplier";
        public static String RANKS = "noOfWinners";
        // Mutable states
        public static String CURRENT_INDEX = "currentIndex";
        public static String SHOW_QUESTION_NUMBER = "showQuestionNumber";
        public static String SHOW_ANSWER = "showAnswer";
        public static String COUNT_DOWN = "countDown";
        public static String SHOW_AD = "showAd";
        public static String AD_COUNT_DOWN = "adCountDown";

    }

    public static class PlayerKeys
    {
        public static String NAME = "name";
        public static String IMAGE_NAME = "imageName";
        public static String USER_TYPE = "userType";
        public static String MATCHES_PLAYED = "matchesPlayed";
        public static String REMAINING_BALANCE = "remainingBalance";
        public static String BALANCE_CLAIMS = "balanceClaims";
        public static String LAST_MATCH_SCORE = "lastMatchScore";
        public static String LAST_MATCH_EARNED = "lastMatchEarned";
        public static String POINTS_SCORED = "pointsScored";
        public static String TOTAL_EARNED = "totalEarned";
        public static String TOTAL_SCORE = "totalScore";
        public static String WEEKLY_SCORE = "weeklyScore";
        public static String LAST_WEEK_SCORE = "lastWeekScore";
        public static String PRIZES_WON = "prizesWon";
        public static String IS_NAME_CHANGED = "isNameChanged";
    }

    public static class ConfigKeys
    {
        public static String IS_PRODUCTION = "isProduction";
        public static String NOTIFICATION_TIME = "notificationTime";
        public static String NOTIFICATION_TITLE = "notificationTitle";
        public static String NOTIFICATION_MESSAGE = "notificationMessage";
    }

    public static class GameplaySettings
    {
        public static int ANSWER_TIME_END = -5;
    }
}
