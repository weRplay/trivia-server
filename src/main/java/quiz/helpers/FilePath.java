package quiz.helpers;

import quiz.config.GameConfig;

public class FilePath
{
    private static String fbConfigPath;
    private static String gameExceptionLogFile;
    private static String pushNotificationLogFile;

    public static String GetFirebaseConfigFile ()
    {
        if (GameConfig.isDevMode)
            fbConfigPath = "src/firebaseConfig.json";
        else
            fbConfigPath = "/home/ubuntu/javaServer/src/firebaseConfig.json";

        return fbConfigPath;
    }

    public static String getGameExceptionLogFile ()
    {
        if (GameConfig.isDevMode)
            gameExceptionLogFile = "logs/ExceptionsLog.txt";
        else
            gameExceptionLogFile = "/home/ubuntu/javaServer/logs/ExceptionsLog.txt";

        return gameExceptionLogFile;
    }

    public static String getPushNotificationLogFile ()
    {
        if (GameConfig.isDevMode)
            pushNotificationLogFile = "logs/pushNotificationLog.txt";
        else
            pushNotificationLogFile = "/home/ubuntu/javaServer/logs/pushNotificationLog.txt";

        return pushNotificationLogFile;
    }

    public static String getCustomLogFile ()
    {
        return "logs/uniqueEasypaisa.txt";
    }
}
