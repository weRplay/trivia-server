package quiz.helpers;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.*;
import java.util.stream.Collectors;

public class ConversionHelper
{
    private static Pattern regex;
    private static String pattern = "<\\|trivia-telenor\\|\\/>";
    private static String replacement = "";

    public static String boolToString (boolean value)
    {
        return Boolean.toString (value);
    }

    public static boolean stringToBool (String value)
    {
        switch (value)
        {
            case "true":
                return true;

            case "false":
                return false;

            case "True":
                return true;

            case "False":
                return false;

            default:
                return false;
        }
    }

    public static int stringToInt (String value)
    {
        return Integer.parseInt (value);
    }

    public static float stringToFloat (String value)
    {
        float returnValue = Float.valueOf (value);
        return returnValue;
    }

    public static long stringToLong (String value)
    {
        return Long.parseLong (value);
    }

    public static int longToInt (long value)
    {
        return Integer.parseInt (Long.toString (value));
    }

    public static String intToString (int value)
    {
        return Integer.toString (value);
    }

    public static int secToMillisec (int value)
    {
        return value * 1000;
    }

    public static long secToMillisec (long value)
    {
        return value * 1000;
    }

    public static String truncateNumber (String value)
    {
        Pattern regex = Pattern.compile ("[^+|0-9]+");
        Matcher matcher = regex.matcher (value);
        return matcher.replaceAll ("");
    }

    public static String removeHiddenCharacters (String value)
    {
        value = value.toLowerCase ();
        Pattern regex = Pattern.compile ("[^a-z0-9]+");
        Matcher matcher = regex.matcher (value);
        return matcher.replaceAll ("");
    }

    public static String decodeString (String data)
    {
        System.out.println ("Debug: decode string. before: "+ data);
        String result;
        byte [] base64DecodedBytes = Base64.getDecoder().decode (data);
        String decodedString = new String (base64DecodedBytes, StandardCharsets.UTF_8);
        regex = Pattern.compile (pattern);
        Matcher matcher = regex.matcher (decodedString);
        result = matcher.replaceAll (replacement);

        System.out.println ("Debug: decode string. after: "+ result);
        return result;
    }

    public static float sumOfAllPowers (float x, int exp)
    {
        float sum = 0;
        for (int i = 0; i < exp; i++)
        {
            sum += Math.pow (x, i);
        }

        return sum;
    }

    public static Map<String, HashMap<String, Object>> invertMap (Map<String, HashMap<String, Object>> playersDataMap)
    {
        Comparator<String> comp = Comparator.comparing(k->k.length());
        comp = comp.thenComparing((a,b)->a.compareTo(b));
        comp = comp.reversed();

        Map<String, HashMap<String, Object>> inversedPlayerDataMap = new LinkedHashMap<>();
        inversedPlayerDataMap = new TreeMap<>(comp);
        inversedPlayerDataMap.putAll(playersDataMap);

        return inversedPlayerDataMap;
    }

    public static Map<String, HashMap<String, Object>> invertMapUsingList (Map<String, HashMap<String, Object>> playersDataMap)
    {
        Map<String, HashMap<String, Object>> inversedPlayerDataMap = new LinkedHashMap<> ();
        List<String> reverseOrderedKeys = new ArrayList<String>(playersDataMap.keySet());
        Collections.reverse(reverseOrderedKeys);
        for (String key : reverseOrderedKeys)
        {
            inversedPlayerDataMap.put (key, playersDataMap.get(key));
        }

        return inversedPlayerDataMap;
    }

    public static Map<String, Object> flatternMap (Map<String, HashMap<String, Object>> playersDataMap )
    {
        Map<String, Object> flatternMap = new HashMap<> ();
        for (Map.Entry<String, HashMap<String, Object>> player : playersDataMap.entrySet ())
        {
            flatternMap.put (player.getKey (), player.getValue ());
        }
        return flatternMap;
    }

}
