package quiz;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import quiz.controllers.*;
import quiz.helpers.FilePath;
import quiz.helpers.Constant;
import quiz.helpers.HelperScript;

import java.io.FileInputStream;
import java.io.IOException;

public class MainApplication
{
    // Controllers Reference
    private static TimerController timerController;
    private static GameScheduleController gameScheduleController;

    public static PushNotificationController pushNotificationController;
    public static LeaderboardController leaderboardController;

    // Db References
    public static FirebaseDatabase firebaseDb;
    public static DatabaseReference gameConfigDbRef;
    public static DatabaseReference nextGamesDbRef;
    public static DatabaseReference currentGameDbRef;
    public static DatabaseReference currentGamePlayersDbRef;
    public static DatabaseReference playedGamesDbRef;
    public static DatabaseReference playedGamekeysDbRef;
    public static DatabaseReference playedGamesDebugDbRef;
    public static DatabaseReference playedGameDebugkeysDbRef;
    public static DatabaseReference usersDbRef;
    public static DatabaseReference leaderboardDbRef;


    public static void main (String[] args) throws IOException, InterruptedException
    {
        System.out.println ("main application ");

        // Initialize Firebase
        initFirebase ();

        // Initialize database references
        initFirebaseDbReferences ();

        // Instantiate Controllers
        gameScheduleController = new GameScheduleController ();
        timerController = new TimerController ();

//        HelperScript.TestHashmap ();
//        HelperScript.RemoveGuestUsers ();
//        HelperScript.MakeUsernamesUnique ();
//        HelperScript.TruncateUsersMobileNumbers ();
//        CashoutController cashoutController = new CashoutController();
    }

    private static void initFirebase () throws IOException, InterruptedException
    {
        System.out.println ("initFirebase ()"+ FilePath.GetFirebaseConfigFile ());
        FileInputStream firebaseConfigFile = new FileInputStream (FilePath.GetFirebaseConfigFile ());
        FirebaseOptions fbOptions = new FirebaseOptions.Builder ()
            .setCredentials (GoogleCredentials.fromStream (firebaseConfigFile))
            .setDatabaseUrl (Constant.DbNodes.DB_URL)
            .build ();

        FirebaseApp.initializeApp (fbOptions);

    }

    private static void initFirebaseDbReferences ()
    {
        firebaseDb = FirebaseDatabase.getInstance ();
        gameConfigDbRef = firebaseDb.getReference ("/gameConfig");
        nextGamesDbRef = firebaseDb.getReference ("/nextGames");
        currentGameDbRef = firebaseDb.getReference ("/currentGame");
        currentGamePlayersDbRef = firebaseDb.getReference ("/currentGame/players");
        playedGamesDbRef = firebaseDb.getReference ("/playedGames/");
        playedGamekeysDbRef = firebaseDb.getReference ("/playedGameKeys");
        playedGamesDebugDbRef = firebaseDb.getReference ("/playedGamesDebug");
        playedGameDebugkeysDbRef = firebaseDb.getReference ("/playedGameDebugKeys");
        usersDbRef = firebaseDb.getReference ("/users/");
        leaderboardDbRef = firebaseDb.getReference ("/leaderboard/");
    }

}
